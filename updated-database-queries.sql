
database name - vehicle_sales_service

/* Personal Details */

create table personal_details(
personId int primary key auto_increment NOT NULL,
name varchar(50),contactNo varchar(25),emailId varchar(50),
password varchar(250),address varchar(100),city varchar(20),
state varchar(20),country varchar(50),pincode varchar(10),
role varchar(20),isActive int(10) )

/* Vendor Details */
create table vendor_details(
vendorId int primary key auto_increment NOT NULL, 
partsName varchar(100),
partsCost decimal(50,2),
partDeliveryDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
personId int,
CONSTRAINT FK_VENDOR_PERSON_ID FOREIGN KEY (personId) REFERENCES personal_details(personId)
);
/*  service type  */
create table service_type(
serviceTypeId int primary key auto_increment NOT NULL,
serviceType varchar(100),
costing decimal(50,2)
);

/*  Customer Details */
create table customer_details(
custId int primary key auto_increment NOT NULL,
personId int,
CONSTRAINT FK_CUSTOMER_PERSON_ID FOREIGN KEY (personId) REFERENCES personal_details(personId)
);

/*  vehicle Details */
create table vehicle_details(
vehId int primary key auto_increment NOT NULL,
vehName varchar(50),
vehModel varchar(50),
vehType varchar(15),
vehCost decimal(50,2),
purchaseDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
status varchar(10),
image varchar(50),
vehNo varchar(20)
);

/*  showroom Details */
create table showroom_details(
showroomId int primary key auto_increment NOT NULL,
showroomName varchar(50),
contactNo varchar(20),
address varchar(100),
city varchar(25),
state varchar(25)
);

/*  payment Details */
create table payment_details(
paymentId int primary key auto_increment NOT NULL,
paymentType varchar(50),
paymentStatus varchar(15),
totalAmount decimal(50,2)
);

/*  servicing Details */
create table servicing_details(
serviceId int primary key auto_increment NOT NULL,
vehId int(10),
custId int(10),
showroomId int(10),
serviceCost decimal(50,2),
serviceDate timestamp,
deliveryDate timestamp,
serviceTypeId int(10),
serviceTime timestamp,
status varchar(10),
paymentId int(10),
CONSTRAINT FK_SERVICE_VEHICLE_ID FOREIGN KEY (vehId) REFERENCES vehicle_details(vehId),
CONSTRAINT FK_SERVICE_CUSTOMER_ID FOREIGN KEY (custId) REFERENCES customer_details(custId),
CONSTRAINT FK_SERVICE_SHOWROOM_ID FOREIGN KEY (showroomId) REFERENCES showroom_details(showroomId),
CONSTRAINT FK_SERVICE_SERVICETYPE_ID FOREIGN KEY (serviceTypeId) REFERENCES service_type(serviceTypeId),
CONSTRAINT FK_SERVICE_PAYMENT_ID FOREIGN KEY (paymentId) REFERENCES payment_details(paymentId)
);

/*  staff Details */
create table staff_details(
staffId int primary key auto_increment NOT NULL,
serviceId int(10),
partPurchaseDate TIMESTAMP,
personId int(10),
CONSTRAINT FK_STAFF_SERVICE_ID FOREIGN KEY (serviceId) REFERENCES servicing_details(serviceId),
CONSTRAINT FK_STAFF_PERSON_ID FOREIGN KEY (personId) REFERENCES personal_details(personId)
);

/*  buy Details */
create table buy_details(
buyId int primary key auto_increment NOT NULL,
vehId int(10),
custId int(10),
showroomId int(10),
vehCost decimal(50,2),
orderDate timestamp,
deliveryDate timestamp,
description varchar(50),
paymentId int(10),
paymentType varchar(50),
status varchar(10),
CONSTRAINT FK_BUY_VEHICLE_ID FOREIGN KEY (vehId) REFERENCES vehicle_details(vehId),
CONSTRAINT FK_BUY_CUSTOMER_ID FOREIGN KEY (custId) REFERENCES customer_details(custId),
CONSTRAINT FK_BUY_SHOWROOM_ID FOREIGN KEY (showroomId) REFERENCES showroom_details(showroomId),
CONSTRAINT FK_BUY_PAYMENT_ID FOREIGN KEY (paymentId) REFERENCES payment_details(paymentId)
);


/*  sales Details */
create table sales_details(
salesId int primary key auto_increment NOT NULL,
vehId int(10),
custId int(10),
showroomId int(10),
vehCost decimal(50,2),
orderDate timestamp,
deliveryDate timestamp,
description varchar(50),
status varchar(10),
paymentId int(10),

CONSTRAINT FK_SALES_VEHICLE_ID FOREIGN KEY (vehId) REFERENCES vehicle_details(vehId),
CONSTRAINT FK_SALES_CUSTOMER_ID FOREIGN KEY (custId) REFERENCES customer_details(custId),
CONSTRAINT FK_SALES_SHOWROOM_ID FOREIGN KEY (showroomId) REFERENCES showroom_details(showroomId),
CONSTRAINT FK_SALES_PAYMENT_ID FOREIGN KEY (paymentId) REFERENCES payment_details(paymentId)
);
/* Admin details */
create table admin_details(
adminId int primary key auto_increment NOT NULL,
adminName varchar(25),
userName varchar(25),
password varchar(250),
contactNumber varchar(25)
);










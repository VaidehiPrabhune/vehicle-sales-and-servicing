left to right direction
Customer-->(Login)
Customer-->(Registration)
Customer-->(Change password)
(Sales vehicle registration)<--Customer 
(List of vehicles on sales)<--Customer
(Add to cart)<--Customer
(Servicing vehicle registration)<--Customer 
(Book appoinment)<--Customer
(Bill generation)<--Customer
(View history)<--Customer

left to right direction
Staff-->(Login)
Staff-->(Registration)
Staff-->(Change password)
(View schedule and appoinments)<--Staff
(Manage schedule)<--Staff
(View vehicle parts)<--Staff
(Place order)<--Staff
(View history)<--Staff

left to right direction
(Login)<--Vendor
(Registration)<--Vendor
(Change password)<--Vendor
Vendor-->(View Order Details)
Vendor-->(Manage Orders)
Vendor-->(Purchase Details)
Vendor-->(Delivery Details)

left to right direction
(Login)<--Admin
(Registration)<--Admin
(Change password)<--Admin
Admin-->(Manage Customers)
Admin-->(Manage Staff)
Admin-->(Manage Vendors)
Admin-->(Manage Payment)
Admin-->(View All Details)
Admin-->(Update Details)

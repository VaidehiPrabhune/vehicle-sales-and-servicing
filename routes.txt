vehicle-sale-service
------------------------------------------------------------------------------------------------------------------------------------------------------------------
customer-front
------------------------------------------------------------------------------------------------------------------------------------------------------------

1)profile management
      - get the profile
      - update profile
      - address management
        - add address
        - update address
        - remove address
      - change password
      - forgot password
      - logout
      - delete an account
2)signup
3)signin    
4)contact us
5)about us
6)vehicle management
     	- sale vehicle 
	- buy vehicle 
		 -search the vehicle
      		- list the vehicle
      		- filter vehicle
	- service vehicle 
		-list showrooms
		-book appointment




------------------------------------------------
- forgot password
  - email
  - generate an OTP (random number of 4 digits)
  - send the OTP to yours email
  - reset the email



-----------------------------------------------------------------------------------------------------------------------------------------------------
Admin-panel
-----------------------------------------------------------------------------------------------------------------------------------------------------

1)customer management
      - get all customer
      - update customer
        - suspend/block customer
        - unblock customer
      - delete customer
2)staff management
      - get all staff
      - update staff
        - suspend/block staff
        - unblock staff
      - delete staff
3)vendor management
      - get all vendor
      - update vendor
        - suspend/block vendor
        - unblock vendor
      - delete vendor
4)sales management
      - list all sold vehicles
      - update status of vehicles like sold or not
        - update delivery status
        - update sales
      - delete sales
5)service management
      - list all service
      - update service
        - update delivery service
        - update service
      - delete service
6)buy management
      - list all unsold vehicles
      - update buy
        - update delivery buy
        - update buy
      - delete buy
7)signup
8)signin

-----------------------------------------------------------------------------------------------------------------------------------------------------
staff-panel
--------------------------------------------------------------------------------------------------------------------------------------------
1)part-order management
      - list all order
      - update order
        - update delivery status       
      - delete order
2)schedule management
      - get schedule
3)signup
4)signin



-----------------------------------------------------------------------------------------------------------------------------------------------------
vendor-panel
--------------------------------------------------------------------------------------------------------------------------------------------
1)order management
      - list all order
      - update order
        - update delivery status
      - delete order
2)part management
      - list all parts
      - update parts
        - update parts cost
      - delete parts
3)signup
4)signin
    
-----------------------------------------------------------------------------------------------------------------------------------------------------


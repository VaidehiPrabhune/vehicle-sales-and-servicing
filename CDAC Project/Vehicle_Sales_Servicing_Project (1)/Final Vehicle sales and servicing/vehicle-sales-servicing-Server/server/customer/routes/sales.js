const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const router = express.Router()
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const config = require('../../config')


//------------------------sale(display customer and vehicle details) GET---------------------------------------------

router.get('/customerAndVehicleDetails', (request, response) => { 
  const statement = `select cd.custId,pd.personId,pd.name,pd.contactNo,pd.emailId,
  pd.password,pd.address,pd.city,pd.state,pd.country,pd.pincode,pd.role,pd.isActive,cd.vehName,cd.vehModel,cd.vehType,cd.vehCost,
  cd.purchaseDate,cd.status,cd.vehNo 
  from personal_details pd INNER JOIN customer_vehicle_details cd
  ON pd.personId=cd.personId where pd.role='customer'`
db.query(statement, (error, data) => {
  response.send(utils.createResult(error, data))
})

})
//----insert personal details and vehicle details done in profile.js---------

//--------------------------------insert sales details POST-------------------------------------------
router.post('/insertSalesDetails/:custid', (request, response) => {
    const {description,showroomName, paymentType,paymentStatus,totalAmount} = request.body
      const { custid } = request.params
    const statement = `insert into sales_details(custId,description,showroomName,
                        paymentType,paymentStatus,totalAmount)
                        values ('${custid}','${description}','${showroomName}','${paymentType}',
                        '${paymentStatus}','${totalAmount}'
                        )`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error,data))
    }) 
  
  })

//--------------------------------update sales details POST-------------------------------------------
router.put('/updateSalesDetails/:custid', (request, response) => {
  const token = request.headers['token']

  try {
    const data = jwt.verify(token, config.secret)
    const { custid } = request.params
    const { description,showroomName,paymentType,paymentStatus,totalAmount} = request.body
    const statement = `update sales_details set description='${description}',showroomName='${showroomName}',
    paymentType='${paymentType}',paymentStatus='${paymentStatus}',totalAmount='${totalAmount}'
     where custId = '${custid}'`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })

  } catch (ex) {
    response.status(401)
    response.send('you are not allowed to access this API')
  }
})

//--------------------------------delete sales details POST-------------------------------------------
 
router.delete('/deleteSalesDetails/:custid', (request, response) => {
  const token = request.headers['token']
  try {

    const data = jwt.verify(token, config.secret)
    const { custid } = request.params
    const statement = `delete from sales_details where custId = '${custid}'`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })

  } catch (ex) {
    response.status(401)
    response.send('you are not allowed to access this API')
  }
})













module.exports = router
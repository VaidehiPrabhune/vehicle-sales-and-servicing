const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const router = express.Router()
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const config = require('../../config')
       
// multer: used for uploading document
const multer = require('multer')
const upload = multer({ dest: 'images/' })


//------------------------customer Details GET---------------------------------------------

router.get('/customerDetails', (request, response) => { 
       
        const statement = `select pd.name,pd.contactNo,pd.emailId,
        pd.address,pd.city,pd.state,pd.country,pd.pincode,cd.custId
        from personal_details pd INNER JOIN  customer_vehicle_details cd
        ON pd.personId=cd.personId where pd.role='customer' and cd.personId='${request.userId}'`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
    
  })
  
  
 //------------------------DELETE personal details------------------------------------------
 
 router.delete('/deletePersonalDetails/:id', (request, response) => {
    const token = request.headers['token']
    try {
  
      const data = jwt.verify(token, config.secret)
      const { id } = request.params
      const statement = `delete from personal_details  where personId = '${id}'`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
  
    } catch (ex) {
      response.status(401)
      response.send('you are not allowed to access this API')
    }
  })

   //------------------------Get personal details------------------------------------------
 
   router.get('/getPersonalDetails/:personId', (request, response) => { 
    const { personId } = request.params
  const statement = `select * from personal_details where personId='${personId}' and role='customer'`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })

})


  //-----------------------update personal details----------------------------------------
 
  router.put('/updatePersonalDetails/:id', (request, response) => {
    const token = request.headers['token']
  
    try {
      const data = jwt.verify(token, config.secret)
      const { id } = request.params
      const { name,contactNo,address,city,state,country,pincode } = request.body
      const statement = `update personal_details set name='${name}',contactNo='${contactNo}'
      ,address='${address}',city='${city}',state='${state}',country='${country}',pincode='${pincode}' where personId = '${id}'`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
  
    } catch (ex) {
      response.status(401)
      response.send('you are not allowed to access this API')
    }
  })


  //-----------------------update customer vehicle details----------------------------------------  
  router.put('/updateCustomerVehicleDetails', (request, response) => {
    const token = request.headers['token']
  
    try {
      const data = jwt.verify(token, config.secret)
      const { custId,vehName,vehModel,vehType,vehCost,purchaseDate,status,image,vehNo } = request.body
      const statement = `update customer_vehicle_details set personId='${request.userId}',vehName='${vehName}'
      ,vehModel='${vehModel}',vehType='${vehType}',vehCost=${vehCost},purchaseDate='${purchaseDate}',status='${status}',
      image='${image}',vehNo='${vehNo}' where custId=${custId}`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
  
    } catch (ex) {
      response.status(401)
      response.send('you are not allowed to access this API')
    }
  })

  //------------------------insert customer vehicle details------------  
  router.post('/insertCustomerVehicleDetails', (request, response) => {
    const {vehName,vehModel,vehType,vehCost,purchaseDate,vehNo} = request.body 
    // encrypt user's password
    const statement = `insert into customer_vehicle_details(personId,vehName,vehModel
      ,vehType,vehCost,purchaseDate,vehNo) values (
      '${request.userId}','${vehName}','${vehModel}','${vehType}','${vehCost}',
      '${purchaseDate}','${vehNo}'
    )`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error,data))
    }) 
  
  })

  //----------------------upload vehicle image ----------------------------
  router.post('/upload-image', upload.single('vehicleImage'), (request, response) => {
    const fileName = request.file.filename

    const statement = `update customer_vehicle_details set image = '${fileName}' where personId='${request.userId}'`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

  //------------------------DELETE customer details-----------------------------------------
  router.delete('/deleteCustomerDetails/:custId', (request, response) => {
    const token = request.headers['token']
    try {
  
      const data = jwt.verify(token, config.secret)
      const { custId } = request.params
      const statement = `delete from customer_vehicle_details where custId = '${custId}'`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
  
    } catch (ex) {
      response.status(401)
      response.send('you are not allowed to access this API')
    }
  })

















  module.exports = router
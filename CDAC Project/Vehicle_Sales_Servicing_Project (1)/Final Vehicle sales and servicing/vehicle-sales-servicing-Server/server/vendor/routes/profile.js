const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const router = express.Router()
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const config = require('../../config')
           

//------------------------vendorDetails GET---------------------------------------------

router.get('/vendorDetails', (request, response) => { 
        const statement = `select pd.personId,pd.name,pd.contactNo,pd.emailId,pd.address,pd.city,pd.state,pd.country,pd.pincode
        from personal_details pd INNER JOIN  vendor_details vd
        ON pd.personId=vd.personId where pd.role='vendor' and pd.personId='${request.userId}'`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
   
  })

  router.get('/vendorOrderDetails', (request, response) => { 

    const statement = `select vd.personId,vd.partsName,vd.partsCost,vd.partDeliveryDate,vd.quantity,vd.deliveryStatus,vd.quantity*vd.partsCost as 'totalAmount'
    from vendor_details vd`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })

})

// const statement = `select vd.vendorId,vd.partsName,vd.partsCost,vd.partDeliveryDate,vd.personId,vd.quantity,vd.deliveryStatus,vd.quantity*vd.partsCost as 'totalAmount'
//     from personal_details pd INNER JOIN  vendor_details vd
//     ON pd.personId=vd.personId where pd.role='vendor' and pd.personId='${request.userId}'`
  
  
  //-------------------------vendor Details POST-----------------------------------
  router.post('/vendorInsert', (request, response) => {
    
    // encrypt user's password
    const {partsName,partsCost,partDeliveryDate,personId,quantity,deliveryStatus,totalAmount} = request.body
    
    const statement = `insert into vendor_details(partsName,partsCost,partDeliveryDate,personId,quantity,deliveryStatus,totalAmount) values (
      '${partsName}','${partsCost}','${partDeliveryDate}','${personId}','${quantity}','${deliveryStatus}','${totalAmount}'
    )`
    console.log(statement)
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error,data))
    }) 
  
  })

 //------------------------DELETE personal details------------------------------------------
 
 router.delete('/deletePersonalDetails/:id', (request, response) => {
    const token = request.headers['token']
    try {
  
      const data = jwt.verify(token, config.secret)
      const { id } = request.params
      const statement = `delete from personal_details  where personId = '${id}'`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
  
    } catch (ex) {
      response.status(401)
      response.send('you are not allowed to access this API')
    }
  })


  //------------------------DELETE vendor details------------------------------------------
 
 router.delete('/deleteVendorDetails/:id', (request, response) => {
    const token = request.headers['token']
    try {
  
      const data = jwt.verify(token, config.secret)
      const { id } = request.params
      const statement = `delete from vendor_details  where vendorId = '${id}'`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
  
    } catch (ex) {
      response.status(401)
      response.send('you are not allowed to access this API')
    }
  })



  //-----------------------update personal details----------------------------------------
 
  // router.put('/updatePersonalDetails/:id', (request, response) => {
  //   const token = request.headers['token']
  
  //   try {
  //     const data = jwt.verify(token, config.secret)
  //     const { id } = request.params
  //     const { name,contactNo,address,city,state,country,pincode } = request.body
  //     const statement = `update personal_details set name='${name}',contactNo='${contactNo}'
  //     ,address='${address}',city='${city}',state='${state}',country='${country}',pincode='${pincode}' where personId = '${id}'`
  //     db.query(statement, (error, data) => {
  //       response.send(utils.createResult(error, data))
  //     })
  
  //   } catch (ex) {
  //     response.status(401)
  //     response.send('you are not allowed to access this API')
  //   }
  // })

  router.put('/updatePersonalDetails', (request, response) => {
    const token = request.headers['token']
  
    try {
      const data = jwt.verify(token, config.secret)
      const { name,contactNo,address,city,state,country,pincode } = request.body
      const statement = `update personal_details set name='${name}',contactNo='${contactNo}'
      ,address='${address}',city='${city}',state='${state}',country='${country}',pincode='${pincode}' where personId = '${request.userId}'`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
  
    } catch (ex) {
      response.status(401)
      response.send('you are not allowed to access this API')
    }
  })

  //-----------------------update vendor details----------------------------------------


  router.put('/updateVendorOrderDetails', (request, response) => {
    const token = request.headers['token']
  
    try {
      const data = jwt.verify(token, config.secret)
      const {partsCost,partDeliveryDate,quantity,deliveryStatus} = request.body 
      const statement = `update vendor_details set partsCost='${partsCost}',partDeliveryDate='${partDeliveryDate}',quantity='${quantity}',deliveryStatus='${deliveryStatus}',totalAmount='${partsCost}'*'${quantity}' where personId = '${request.userId}'`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
  
    } catch (ex) {
      response.status(401)
      response.send('you are not allowed to access this API')
    }
  })



  module.exports = router
const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const router = express.Router()
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const config = require('../../config')

//---------------------------Get servicing details--------------------
router.get('/VehicleServiceDetails', (request, response) => { 
  const token = request.headers['token']
  
        const statement = `select st.serviceId ,pd.name as "Customer Name", sr.serviceDate, sr.deliveryDate,sr.status
        from staff_details st INNER JOIN servicing_details sr 
        ON st.serviceId =sr.serviceId 
        INNER JOIN customer_vehicle_details cd on cd.custId=sr.custId 
        INNER JOIN personal_details pd on cd.personId=pd.personId`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
    
   
  })

//----------------------update servicing status(done/notdone)-------------
  router.put('/custoUpdateStatus/:id', (request, response) => {
      const { id } = request.params
         
      // encrypt user's password
      const statement = `update servicing_details set status=1 where serviceId= '${id}' and status=0`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error,data))
      }) 
    
    })

router.put('/custoUpdateStatus/:id/:status', (request, response) => {
  const {id, status} = request.params
  const statement = `update servicing_details set 
  status = ${status}
    where serviceId = ${id}`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
}) 

//--------------------------delete servicing of customer------------------
router.delete('/service/:id', (request, response) => {
  const { id } = request.params
 
  // encrypt user's password
  const statement = `delete from servicing_details  where serviceId= '${id}' `
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error,data))
  }) 

})


module.exports = router
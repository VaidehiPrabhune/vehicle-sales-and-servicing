const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const router = express.Router()
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const config = require('../../config')

//------------------------sale GET(display list of sold vehicles)---------------------------------------------

router.get('/custoDetails', (request, response) => { 
    const statement = `select cd.custId,pd.personId,pd.name,cd.vehName,cd.vehModel,cd.vehType,cd.vehCost,
    cd.purchaseDate,cd.status,cd.vehNo 
    from personal_details pd INNER JOIN customer_vehicle_details cd
    ON pd.personId=cd.personId where cd.status='notsold'`
    db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
  
})
//-----------------update custoomer vehicle status---------------------------------- 
router.put('/customerVehicleUpdateStatus/:id', (request, response) => {
    const { id } = request.params   
    // encrypt user's password
    const statement = `update customer_vehicle_details set status='sold' where custId= '${id}' and status='notsold'`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error,data))
    }) 
  
  })


module.exports = router




const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const router = express.Router()
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const config = require('../../config')

//-------------------get vendor details------------------------
router.get('/vendorDetails', (request, response) => { 
    const statement = `select pd.personId,pd.name,pd.contactNo,pd.emailId,pd.address,pd.city,pd.state,pd.country,pd.pincode
    from personal_details pd where pd.role='vendor'`
    db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
    })
    
    }) 

//-----------------------update vendor details----------------------------------------
router.put('/updateVendorDetails/:id', (request, response) => {
    const token = request.headers['token']
  
    try {
      const data = jwt.verify(token, config.secret)
      const { id } = request.params
      const { partsName,partsCost,partDeliveryDate,quantity,deliveryStatus } = request.body
      const statement = `update vendor_details set partsName='${partsName}',partsCost='${partsCost}'
      ,partDeliveryDate='${partDeliveryDate}',quantity='${quantity}',deliveryStatus='${deliveryStatus}' where vendorId = '${id}'`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
  
    } catch (ex) {
      response.status(401)
      response.send('you are not allowed to access this API')
    }
  })

//------------------------DELETE vendor details------------------------------------------
 
 router.delete('/deleteVendorDetails/:id', (request, response) => {
    const token = request.headers['token']
    try {
  
      const data = jwt.verify(token, config.secret)
      const { id } = request.params
      const statement = `delete from vendor_details  where vendorId = '${id}'`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
  
    } catch (ex) {
      response.status(401)
      response.send('you are not allowed to access this API')
    }
  })


//-------------------------vendor Details POST-----------------------------------
 router.post('/vendorInsert', (request, response) => {
    
    // encrypt user's password
    const {partsName,partsCost,partDeliveryDate,personId,quantity,deliveryStatus,totalAmount} = request.body
    
    const statement = `insert into vendor_details(partsName,partsCost,partDeliveryDate,personId,quantity,deliveryStatus,totalAmount) values (
      '${partsName}','${partsCost}','${partDeliveryDate}','${personId}','${quantity}','${deliveryStatus}','${totalAmount}'
    )`
    console.log(statement)
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error,data))
    }) 
  
  }) 









module.exports = router
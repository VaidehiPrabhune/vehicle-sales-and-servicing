const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const router = express.Router()
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const config = require('../../config')

//------------------------Buy GET(get list of unsold vehicles)---------------------------------------------
router.get('/BuyVehicleDetails', (request, response) => { 
  const statement = `select cd.custId,pd.personId,pd.name,cd.vehName,cd.vehModel,cd.vehType,cd.vehCost,
  cd.purchaseDate,cd.status,cd.vehNo 
  from personal_details pd INNER JOIN customer_vehicle_details cd
  ON pd.personId=cd.personId where cd.status='unsold' or cd.status='sold'`
  db.query(statement, (error, data) => {
  response.send(utils.createResult(error, data))
})

})
  
















module.exports = router
const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const router = express.Router()
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const config = require('../../config')


// --------------------staff Signup-------------------
router.post('/signup', (request, response) => {
    const {serviceId ,name,contactNo,emailId,password,address} = request.body
    
    // encrypt user's password
    const statement = `insert into staff_details(serviceId ,name,contactNo,emailId,password,address) values (
        '${serviceId}' ,'${name}','${contactNo}','${emailId}','${crypto.SHA256(password)}','${address}')`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error,data))
    }) 
  
  })


  //------------------------------------staff signin---------------------------------------
  router.post('/signin', (request, response) => {
    const { emailId, password } = request.body
    const statement = `select serviceId ,name,contactNo,address from staff_details 
                      where emailId = '${emailId}' and password = '${crypto.SHA256(password)}'`
    db.query(statement, (error, users) => {
      if (error) {
        response.send({ status: 'error', error: error })
      } else {
        if (users.length == 0) {
          // user does not exist
          response.send({ status: 'error', error: 'user does not exist' })
        } else {
          // user exists
          const user = users[0]
          const token = jwt.sign({id: user['userId']},config.secret)
          response.send(utils.createResult(error,
            {
              name: user['name'],
              emailId: user['emailId'],
              token: token
            }
          ))
          
        }
      }
    })
  })
  
//----------------------------------------Get staff details-----------------------------------
  router.get('/staffDetails', (request, response) => { 
    const token = request.headers['token']
    try {
          const statement = `select st.serviceId ,st.name,st.contactNo,st.emailId,st.password,st.address,sr.status 
          from staff_details st INNER JOIN servicing_details sr
          ON st.serviceId =sr.serviceId`
        db.query(statement, (error, data) => {
          response.send(utils.createResult(error, data))
        })
      }catch (ex) {
        response.status(401)
        response.send('you are not allowed to access this API')
      }
     
    })

//----------------------------get staff schedule-----------------------  
router.get('/schedule/:id', (request, response) => { 
    const token = request.headers['token']
    try {
          const statement = `select st.name,st.serviceId , sr.custId, sr.serviceDate, sr.deliveryDate,sr.status 
          from staff_details st INNER JOIN servicing_details sr
          ON st.serviceId =sr.serviceId  where staffId = '${id}'`
        db.query(statement, (error, data) => {
          response.send(utils.createResult(error, data))
        })
      }catch (ex) {
        response.status(401)
        response.send('you are not allowed to access this API')
      }
     
    })












module.exports = router

/* Personal Details */

create table personal_details(
personId int primary key auto_increment NOT NULL,
name varchar(50),
contactNo varchar(25),
emailId varchar(50),
password varchar(250),
address varchar(100),
city varchar(20),
state varchar(20),
country varchar(50),
pincode varchar(10),
role varchar(20),
isActive int(10) );

/*  Customer Details */
create table customer_details(
custId int primary key auto_increment NOT NULL,
personId int,
vehName varchar(50),
vehModel varchar(50),
vehType varchar(15),
vehCost decimal(10,2),
purchaseDate date,
status varchar(10),
image Varchar(50),
vehNo Varchar(20),
CONSTRAINT FK_CUSTOMER_PERSON_ID FOREIGN KEY (personId) REFERENCES personal_details(personId) on update cascade on delete cascade
);

/* Admin details */
create table admin_details(
adminId int primary key auto_increment NOT NULL,
personId int,
CONSTRAINT FK_ADMIN_PERSON_ID FOREIGN KEY (personId) REFERENCES personal_details(personId) on update cascade on delete cascade
);

/*  servicing Details */
create table servicing_details(
serviceId int primary key auto_increment NOT NULL,
custId int(10),
serviceCost decimal(50,2),
serviceDate timestamp DEFAULT CURRENT_TIMESTAMP,
deliveryDate timestamp DEFAULT CURRENT_TIMESTAMP,
serviceTime timestamp DEFAULT CURRENT_TIMESTAMP,
status varchar(10),
serviceType varchar(10),
costing decimal(10),
paymentType varchar(50),
paymentStatus varchar(15),
totalAmount decimal(50),
showroomName varchar(50),
CONSTRAINT FK_SERVICE_CUSTOMER_ID FOREIGN KEY (custId) REFERENCES customer_details(custId) on update cascade on delete cascade
);

/*  sales Details */
create table sales_details(
salesId int primary key auto_increment NOT NULL,
custId int(10),
orderDate timestamp DEFAULT CURRENT_TIMESTAMP,
deliveryDate timestamp DEFAULT CURRENT_TIMESTAMP,
description varchar(50),
showroomName varchar(50),
paymentType varchar(50),
paymentStatus varchar(15),
totalAmount decimal(50,2),
CONSTRAINT FK_SALES_CUSTOMER_ID FOREIGN KEY (custId) REFERENCES customer_details(custId) on update cascade on delete cascade
);


/*  buy Details */
create table buy_details(
buyId int primary key auto_increment NOT NULL,
custId int(10),
vehCost decimal(50,2),
orderDate timestamp DEFAULT CURRENT_TIMESTAMP,
deliveryDate timestamp DEFAULT CURRENT_TIMESTAMP,
description varchar(50),
status varchar(10),
showroomName varchar(50),
paymentType varchar(50),
paymentStatus varchar(15),
totalAmount decimal(50,2),
CONSTRAINT FK_BUY_CUSTOMER_ID FOREIGN KEY (custId) REFERENCES customer_details(custId) on update cascade on delete cascade
);


/*  staff Details */
create table staff_details(
staffId int primary key auto_increment NOT NULL,
serviceId int(10),
partPurchaseDate TIMESTAMP,
name varchar(25),
contactNo varchar(25),
emailId varchar(25),
password varchar(250),
address varchar(100),
CONSTRAINT FK_STAFF_SERVICE_ID FOREIGN KEY (serviceId) REFERENCES servicing_details(serviceId) on update cascade on delete cascade
);

/* Vendor Details */
create table vendor_details(
vendorId int primary key auto_increment NOT NULL, 
partsName varchar(100),
partsCost decimal(50,2),
partDeliveryDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
personId int,
CONSTRAINT FK_VENDOR_PERSON_ID FOREIGN KEY (personId) REFERENCES personal_details(personId) on update cascade on delete cascade
);
















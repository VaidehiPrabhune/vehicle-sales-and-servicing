import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VendordashboardComponent } from './vendordashboard/vendordashboard.component';

import { OrderlistComponent } from './orderlist/orderlist.component';
import { AddPersonComponent } from './add-person/add-person.component';
import { UpdateOrderComponent } from './update-order/update-order.component';
import { ProfileComponent } from './profile/profile.component';





const routes: Routes = [
  {path:'vendordashboard',component:VendordashboardComponent},
  {path:'profile',component:ProfileComponent},
  {path:'orderlist',component:OrderlistComponent},
  {path:'add-person',component:AddPersonComponent},
  {path:'update-order',component:UpdateOrderComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VendorRoutingModule { }

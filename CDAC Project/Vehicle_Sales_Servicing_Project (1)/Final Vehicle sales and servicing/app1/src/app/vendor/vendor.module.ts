import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendorRoutingModule } from './vendor-routing.module';

import { FormsModule } from '@angular/forms';
import { VendordashboardComponent } from './vendordashboard/vendordashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { AddPersonComponent } from './add-person/add-person.component';
import { OrderlistComponent } from './orderlist/orderlist.component';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { UpdateOrderComponent } from './update-order/update-order.component';


@NgModule({
  declarations: [VendordashboardComponent, ProfileComponent, OrderlistComponent, UpdateOrderComponent,AddPersonComponent],
  imports: [
    CommonModule,
    VendorRoutingModule,
    FormsModule,
    HttpClientModule ,// required animations module
    ToastrModule.forRoot(),
  ]
})
export class VendorModule { }

import { VendorService } from '../../vendor.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';


@Component({
  selector: 'app-update-order',
  templateUrl: './update-order.component.html',
  styleUrls: ['./update-order.component.css']
})
export class UpdateOrderComponent implements OnInit {

  partsCost = 0.0
  partDeliveryDate
  quantity = 0
  deliveryStatus = 0
  totalAmount = 0.0

  order = null


  constructor(
    private router: Router,
    private vendorService: VendorService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    const personId = this.activatedRoute.snapshot.queryParams['personId']
    if (personId) {
      // edit vendor
      this.vendorService
        .getOrder()
        .subscribe(response => {
          if (response['status'] == 'success') {
            const orders = response['data']
            if (orders.length > 0) {
              this.order = orders[0]
              this.partsCost = this.order['partsCost']
              this.partDeliveryDate = this.order['partDeliveryDate']
              this.quantity = this.order['quantity']
              this.deliveryStatus = this.order['deliveryStatus']
              this.totalAmount = this.order['totalAmount']

            }
          }
        })
      }
    }

    onUpdate() {

      if (this.order) {
        // edit
        console.log(this.order);
        this.vendorService
          .updateOrder(this.partsCost, this.partDeliveryDate, this.quantity, this.deliveryStatus)
          .subscribe(response => {
            if (response['status'] == 'success') {
              this.router.navigate(['/home/vendor/orderlist'])
            }
          })
      }
    }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StaffserviceService {
  url = 'http://localhost:5000/login'

  constructor(
    private httpClient: HttpClient) { }

    getStaffProfile(personId) {
      // add the token in the request header
      const httpOptions = {
       headers: new HttpHeaders({
         token: sessionStorage['token'],
         role:sessionStorage['role']
       })
     };
     
     return this.httpClient.get(this.url+'/staffprofile/' +personId, httpOptions)
   }

   getAllStaffProfile() {
    // add the token in the request header
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token'],
       role:sessionStorage['role']
     })
   };
   
   return this.httpClient.get(this.url+'/allStaffDetails', httpOptions)
 }

   getStaffSchedule(personId){
    {
      // add the token in the request header
      const httpOptions = {
       headers: new HttpHeaders({
         token: sessionStorage['token'],
         role:sessionStorage['role']
       })
     };
     console.log(personId)
     
     return this.httpClient.get(this.url+'/staffschedule/' +personId, httpOptions)
    
   }
   }



   placeOrder(partName: string, qty: number,personId:number) {
    // add the token in the request header
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token']
     })
   };

   const body = {
    partName: partName,
    qty: qty,
    personId:personId

   }

   console.log(body)
   
   return this.httpClient.post(this.url + "/placeOrder", body, httpOptions)
 }

//----------------------admin login---------------------------------
 

 deletestaff(personid) {
  // add the token in the request header
  const httpOptions = {
   headers: new HttpHeaders({
     token: sessionStorage['token']
   })
 };

 console.log(personid)
 
 return this.httpClient.delete(this.url + "/deleteStaffDetails/" + personid, httpOptions)
}



addStaff(name: string, contactNo : number, emailId: string, password: String,  address : string, city : string,
  state : string,country : string,pincode : number,role : string)
{
  const body = {
    name:name,
    contactNo:contactNo,
    emailId: emailId,
    password: password,
    address:address,
    city:city,
    state:state,
    country:country,
    pincode:pincode,
    role:role
            
  }

  return this.httpClient.post(this.url + '/addstaff', body)
}





updatestaff(id, name: string, contactNo: number,  address:string,city:string,state:string,country:string,pincode:number) {
  // add the token in the request header
  const httpOptions = {
   headers: new HttpHeaders({
     token: sessionStorage['token']
   })
 };

 const body = {
  name:name,
  contactNo:contactNo,  
  address:address,
  city:city,
  state:state,
  country:country,
  pincode:pincode
  
 }
 
 return this.httpClient.put(this.url + "/updateStaffDetails/" + id, body, httpOptions)
}




addService(serviceId: number, personId : number)
{
   const body = {
    serviceId:serviceId,
    personId:personId               
  }
console.log(serviceId)
console.log(personId)
  return this.httpClient.post(this.url + '/addservice', body)
}

}


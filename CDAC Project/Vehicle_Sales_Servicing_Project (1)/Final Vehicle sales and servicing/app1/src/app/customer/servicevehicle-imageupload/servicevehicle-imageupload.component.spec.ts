import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicevehicleImageuploadComponent } from './servicevehicle-imageupload.component';

describe('ServicevehicleImageuploadComponent', () => {
  let component: ServicevehicleImageuploadComponent;
  let fixture: ComponentFixture<ServicevehicleImageuploadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServicevehicleImageuploadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicevehicleImageuploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

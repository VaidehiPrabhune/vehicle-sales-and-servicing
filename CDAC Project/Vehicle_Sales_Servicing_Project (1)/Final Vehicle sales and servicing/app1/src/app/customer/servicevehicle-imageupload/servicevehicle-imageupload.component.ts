import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustomerserviceService } from '../customerservice.service';

@Component({
  selector: 'app-servicevehicle-imageupload',
  templateUrl: './servicevehicle-imageupload.component.html',
  styleUrls: ['./servicevehicle-imageupload.component.css']
})
export class ServicevehicleImageuploadComponent implements OnInit {

  selectedFile = null
  
  constructor(
    private router: Router,
    private toastr : ToastrService,
    private activatedRoute: ActivatedRoute,
    private customerservice: CustomerserviceService) { }

  ngOnInit(): void {
  }

  onImageSelect(event) {
    this.selectedFile = event.target.files[0]
  }

  onUploadImage() {
    this.customerservice
      .uploadImage(this.selectedFile)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.toastr.success(`Vehicle details added successfully.`)
          this.toastr.success(`Thanks ${sessionStorage['name']}. Visit Again !!`)
          this.router.navigate(['/home/customer/servicevehicle'])
        } else {
          console.log(response['error'])
        }
      })
  }

}

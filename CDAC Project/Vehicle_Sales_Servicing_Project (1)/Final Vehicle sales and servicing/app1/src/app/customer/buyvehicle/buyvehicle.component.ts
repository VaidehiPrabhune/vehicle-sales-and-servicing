import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerserviceService } from '../customerservice.service';


@Component({
  selector: 'app-buyvehicle',
  templateUrl: './buyvehicle.component.html',
  styleUrls: ['./buyvehicle.component.css']
})

export class BuyvehicleComponent implements OnInit {

  vehTypes=['all','car','bicycle','bike','bus','truck']
  vehicles=[]
  allVehicles=[]
 personId=0;

  constructor(
    private router :  Router,
    private customerserviceService : CustomerserviceService
  ) { }

 
  ngOnInit(): void {
    this.loadVehicles()
  }

  filterVehicles(event) {
    const vehicletype = event.target.value
    this.vehicles = []
    if (vehicletype == 'all') {
      this.vehicles = this.allVehicles
    } else {
      this.vehicles = this.allVehicles.filter(vehicle => {
        return vehicle['vehType'] == vehicletype
      })
    }
  }

  loadVehicles() {
    this.customerserviceService
      .getVehicles()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.allVehicles = response['data']
          this.vehicles = this.allVehicles
         
        } else {
          console.log(response['error'])
        }
      })
  }

  buyVehicle(vehicle) {
   console.log(vehicle)
    this.router.navigate(['/home/customer/vehicle-alldetails'], {queryParams: {personId: vehicle['personId']}})
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalevehicleComponent } from './salevehicle.component'

describe('SalevehicleComponent', () => {
  let component: SalevehicleComponent;
  let fixture: ComponentFixture<SalevehicleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalevehicleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalevehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

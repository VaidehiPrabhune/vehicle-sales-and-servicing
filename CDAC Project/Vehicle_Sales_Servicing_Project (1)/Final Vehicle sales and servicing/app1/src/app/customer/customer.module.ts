import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerRoutingModule } from './customer-routing.module';

import { FormsModule } from '@angular/forms';
import { CustomerdashboardComponent } from './customerdashboard/customerdashboard.component';
;
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';



import { ServicevehicleComponent } from './servicevehicle/servicevehicle.component';
import { SalevehicleComponent } from './salevehicle/salevehicle.component';
import { CustomerserviceService } from './customerservice.service';
import { ProfileComponent } from './profile/profile.component';
import { VehicleAlldetailsComponent } from './vehicle-alldetails/vehicle-alldetails.component';
import { BuyvehicleComponent } from './buyvehicle/buyvehicle.component';
import { VehicleUploadImageComponent } from './vehicle-upload-image/vehicle-upload-image.component';
import { PaymentDetailsComponent } from './payment-details/payment-details.component';
import { ServicevehicleDetailsComponent } from './servicevehicle-details/servicevehicle-details.component';
import { ServicevehicleImageuploadComponent } from './servicevehicle-imageupload/servicevehicle-imageupload.component';



@NgModule({
  declarations: [CustomerdashboardComponent,ProfileComponent, SalevehicleComponent,ServicevehicleComponent, VehicleAlldetailsComponent, BuyvehicleComponent, VehicleUploadImageComponent, PaymentDetailsComponent, ServicevehicleDetailsComponent, ServicevehicleImageuploadComponent],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    FormsModule,
    HttpClientModule ,// required animations module
    ToastrModule.forRoot(),
  ],
  providers: [
    CustomerserviceService
  ]
})
export class CustomerModule { }

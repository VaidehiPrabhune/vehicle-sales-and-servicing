import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustomerserviceService } from '../customerservice.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  persons  = []
  constructor(
    private router :  Router,
    private toastr : ToastrService,
    private customerserviceService : CustomerserviceService,
    private activatedRoute: ActivatedRoute,
  ) { }


  ngOnInit(): void {
      
    this.customerserviceService
      .getPersonalDetails()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.persons = response['data']
        } else {
          console.log(response['error'])
        }
      })
  
    
  }

  

}


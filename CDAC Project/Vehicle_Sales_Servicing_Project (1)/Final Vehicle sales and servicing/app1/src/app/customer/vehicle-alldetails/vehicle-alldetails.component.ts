import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomerserviceService } from '../customerservice.service';

@Component({
  selector: 'app-vehicle-alldetails',
  templateUrl: './vehicle-alldetails.component.html',
  styleUrls: ['./vehicle-alldetails.component.css']
})
export class VehicleAlldetailsComponent implements OnInit {

   vehicles=[]
  
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private customerservice:CustomerserviceService
  ) { }

  
  ngOnInit(): void {
    const personId = this.activatedRoute.snapshot.queryParams['personId']
    console.log(personId)
    if(personId)
    {
      this.customerservice
      .getSingleVehicleDetails(personId)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.vehicles = response['data']
           
          
        } else {
          console.log(response['error'])
        }
      })
    }
  }


  paymentDetails(vehicle)
  {
    this.router.navigate(['/home/customer/payment-details'], {queryParams: {custId: vehicle['custId']}})
  }
}

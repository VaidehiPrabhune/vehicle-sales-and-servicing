import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustomerserviceService } from '../customerservice.service';
import { Time } from '@angular/common';
import { concat } from 'rxjs';

@Component({
  selector: 'app-servicevehicle',
  templateUrl: './servicevehicle.component.html',
  styleUrls: ['./servicevehicle.component.css']
})
export class ServicevehicleComponent implements OnInit {

  custId: 0
  serviceCost: 0
  serviceDate : Date
  deliveryDate: Date
  serviceTime:Time 
  serviceType: string[] 
  costing  : number[]
  paymentType :''
  totalAmount:0
  showroomName:''

  
  persons  = []
  customer=null;

  constructor(
    private router :  Router,
    private toastr : ToastrService,
    private customerserviceService : CustomerserviceService
  ) { }

  ngOnInit(): void {
    this.customerserviceService
      .getPersonalDetails()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.persons = response['data']
          this.customer=this.persons[0]
        } else {
          console.log(response['error'])
        }
      })


  }
 

  

  onInsertServicingDetails()
  {
    
      this.customerserviceService.insertServceVehicleDetails(this.customer['custId'],this.serviceCost,this.serviceDate
      ,this.deliveryDate,this.serviceTime,this.serviceType,this.costing,this.paymentType,this.totalAmount,this.showroomName).subscribe(response => {
        if(response['status'] == 'success')
        {        
          this.toastr.success(`Vehicle servicng details added successfully.`)
          this.toastr.success(`Please enter your vehicle details.`)
          this.router.navigate(['/home/customer/customerdashboard'])
        }
        else
        {
          console.log(response['error'])
        }
      })
    
  }

  onCancel()
  {
    this.router.navigate(['/home/customer/customerdashboard'])
  }
}
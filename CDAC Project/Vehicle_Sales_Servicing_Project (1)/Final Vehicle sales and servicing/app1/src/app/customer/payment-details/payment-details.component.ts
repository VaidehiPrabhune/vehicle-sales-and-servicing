import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustomerserviceService } from '../customerservice.service';

@Component({
  selector: 'app-payment-details',
  templateUrl: './payment-details.component.html',
  styleUrls: ['./payment-details.component.css']
})
export class PaymentDetailsComponent implements OnInit {

  vehCost=0;
  orderDate;
  deliveryDate;
  showroomName=''
  paymentType=''
  totalAmount=0

  custId=0;
  constructor(
    private router :  Router,
    private toastr : ToastrService,
    private activatedRoute: ActivatedRoute,
    private customerserviceService : CustomerserviceService
  ) { }

  ngOnInit(): void {
    this.onUpdateVehicleDetails()
  }

  onPaymentDone()
  {
     this.custId = this.activatedRoute.snapshot.queryParams['custId']
    this.customerserviceService.insertPaymentDetails(this.custId,this.vehCost,this.orderDate, this.deliveryDate,
       this.showroomName,this.paymentType,this.totalAmount).subscribe(response => {
        if(response['status'] == 'success')
        {        
          this.toastr.success(`Thanks ${sessionStorage['name']}. Visit Again !!`)
          this.router.navigate(['/home/customer/customerdashboard'])
        }
        else
        {
          console.log(response['error'])
        }
      })
  }

  onCancel()
  {
    this.router.navigate(['/home/customer/customerdashboard'])
  }

  onUpdateVehicleDetails()
  {
    this.custId = this.activatedRoute.snapshot.queryParams['custId']
    this.customerserviceService.updateVehicleDetails(this.custId).subscribe(response => {
       if(response['status'] == 'success')
       {        
        this.onPaymentDone()
       }
       else
       {
         console.log(response['error'])
       }
     })
  }

}

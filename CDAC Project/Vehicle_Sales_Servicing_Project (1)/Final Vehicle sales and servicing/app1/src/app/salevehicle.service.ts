import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SalevehicleService {

  url = 'http://localhost:5000/saleVehicle'

  constructor(
    private httpClient: HttpClient) { }
  
  getVehicles() {
     // add the token in the request header
     const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token'],
        role:sessionStorage['role']
      })
    };
    
    return this.httpClient.get(this.url+'/custoDetails', httpOptions)
  }
}


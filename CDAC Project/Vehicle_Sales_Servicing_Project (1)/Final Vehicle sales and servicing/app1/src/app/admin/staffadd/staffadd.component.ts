import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { StaffserviceService } from 'src/app/staffservice.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-staffadd',
  templateUrl: './staffadd.component.html',
  styleUrls: ['./staffadd.component.css']
})
export class StaffaddComponent implements OnInit {

  name: ' '
  contactNo : 0
  emailId: ''
  password:'' 
  address : ' '
  city : ''
  state : ''
  country :'' 
  pincode : 0
  role : ' '

  staff = null

  constructor(
    private router :  Router,
    private toastr : ToastrService,
    private activatedRoute: ActivatedRoute,
    private staffserviceService : StaffserviceService
  ) { }

  ngOnInit(): void {
    

    
  }

 
  OnAdd()
  {
    if(this.name.length == 0)
    {
      this.toastr.error('Enter your name')
    }
    else if(this.contactNo.toString().length == 0)
    {
      this.toastr.error('Enter your contactNo')
    }
    else if(this.emailId.length == 0)
    {
      this.toastr.error('Enter your emailId')
    }
    else if(this.password.length == 0)
    {
      this.toastr.error('Enter your Password')
    }
    else if(this.address.length == 0)
    {
      this.toastr.error('Enter your address')
    }
    else if(this.city.length == 0)
    {
      this.toastr.error('Enter your city')
    }
    else if(this.state.length == 0)
    {
      this.toastr.error('Enter your state')
    }
    else if(this.country.length == 0)
    {
      this.toastr.error('Enter your country')
    }
    else if(this.pincode.toString().length == 0)
    {
      this.toastr.error('Enter your pincode')
    }
    else
    {
      this.staffserviceService.addStaff(this.name,this.contactNo,this.emailId, this.password,this.address,this.city,this.state,this.country,this.pincode,this.role).subscribe(response => {
        if(response['status'] == 'success')
        {
            this.toastr.success('Registered Successfully')
            this.router.navigate(['/home/admin/staff'])
           
        }
        else
        {
          console.log(response['error'])
        }
      })
    }
    }


    


}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { StaffserviceService } from 'src/app/staffservice.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-editstaff',
  templateUrl: './editstaff.component.html',
  styleUrls: ['./editstaff.component.css']
})
export class EditstaffComponent implements OnInit {

  name: ' '
  contactNo : 0
  emailId: ''
  password:'' 
  address : ' '
  city : ''
  state : ''
  country :'' 
  pincode : 0
  role : ' '

  staff = null

  constructor(
    private router :  Router,
    private toastr : ToastrService,
    private activatedRoute: ActivatedRoute,
    private staffserviceService : StaffserviceService
  ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.queryParams['id']
    console.log('t2',id)
    if (id) {
      // edit product
      this.staffserviceService
        .getStaffProfile(id)
        .subscribe(response => {
          if (response['status'] == 'success') {
            const products = response['data']
            if (products.length > 0) {
              this.staff = products[0]
              this.name = this.staff['name']
              this.contactNo = this.staff['contactNo']
              this.address = this.staff['address']
              this.city = this.staff['city']
              this.state = this.staff['state']
              this.country = this.staff['country']
              

            }
          }
        })
    }
  }


  onUpdate() {

    const id = this.activatedRoute.snapshot.queryParams['id']
      // edit
      this.staffserviceService
        .updatestaff(id, this.name,this.contactNo,this.address,this.city,this.state,this.country,this.pincode)
        .subscribe(response => {
          if (response['status'] == 'success') {
            this.router.navigate(['/home/admin/staff'])
          }
        })
   

  }

}

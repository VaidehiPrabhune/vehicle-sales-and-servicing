import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';

import {FormsModule} from '@angular/forms';
import { ProfileComponent } from './profile/profile.component';
import { AdmindashboardComponent } from './admindashboard/admindashboard.component';
import { CustomerComponent } from './customer/customer.component';
import { BuyvehicleComponent } from './buyvehicle/buyvehicle.component';
import { SalevehicleComponent } from './salevehicle/salevehicle.component';
import { ServicevehicleComponent } from './servicevehicle/servicevehicle.component';
import { StaffComponent } from './staff/staff.component';
import { VendorComponent } from './vendor/vendor.component'
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { StaffaddComponent } from './staffadd/staffadd.component';
import { AssignserviceComponent } from './assignservice/assignservice.component';
import { EditstaffComponent } from './editstaff/editstaff.component';

@NgModule({
  declarations: [ProfileComponent, AdmindashboardComponent,
  CustomerComponent, BuyvehicleComponent, SalevehicleComponent, ServicevehicleComponent, StaffComponent, VendorComponent, StaffaddComponent, AssignserviceComponent, EditstaffComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    HttpClientModule ,// required animations module
    ToastrModule.forRoot(),
  ]
})
export class AdminModule { }

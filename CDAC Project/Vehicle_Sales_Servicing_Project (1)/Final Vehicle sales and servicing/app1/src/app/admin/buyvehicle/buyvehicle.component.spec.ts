import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyvehicleComponent } from './buyvehicle.component';

describe('BuyvehicleComponent', () => {
  let component: BuyvehicleComponent;
  let fixture: ComponentFixture<BuyvehicleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuyvehicleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyvehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

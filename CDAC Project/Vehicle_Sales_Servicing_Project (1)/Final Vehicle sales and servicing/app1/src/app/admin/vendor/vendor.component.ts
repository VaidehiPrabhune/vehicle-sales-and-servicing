import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { VendorService } from 'src/app/admin/vendor.service';


@Component({
  selector: 'app-vendor',
  templateUrl: './vendor.component.html',
  styleUrls: ['./vendor.component.css']
})
export class VendorComponent implements OnInit {
  vendors = []

  constructor(
    private router: Router,
    private vendorService: VendorService) { }

  ngOnInit(): void {
    this.loadVendors()
  }

  loadVendors() {
    this.vendorService
      .getVendor()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.vendors = response['data']
        } else {
          console.log(response['error'])
        }
      })
  }

}

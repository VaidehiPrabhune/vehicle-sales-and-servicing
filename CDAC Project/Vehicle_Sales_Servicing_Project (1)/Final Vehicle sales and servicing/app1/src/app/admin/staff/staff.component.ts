import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StaffserviceService } from 'src/app/staffservice.service';
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})
export class StaffComponent implements OnInit {

  staffs = []
  schedules=[]
  constructor(
    private router: Router,
    private toastr: ToastrService,
    private staffService: StaffserviceService) { }

  ngOnInit(): void {
    this.getAllStaffProfile()
  }

  getAllStaffProfile() {
    this.staffService
      .getAllStaffProfile()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.staffs = response['data']
          console.log('sc=',this.staffs)
        } else {
          console.log(response['error'])
        }
      })
  }

  getStaffSchedule(staff) {
      console.log(staff['personId'])
    this.staffService
      .getStaffSchedule(staff['personId'])
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.schedules = response['data']
          console.log(this.schedules)
        } else {
          console.log(response['error'])
        }
      })
  }


  onDelete(staff) {
    this.staffService
      .deletestaff(staff['personId'])
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.toastr.success('staff removed successfully')
          this.getAllStaffProfile()
        }
      })
  }

  addStaff() {
    this.router.navigate(['/home/admin/staffadd'])
  }

  onEdit(staff) {
    console.log('t1',staff['personId'])
      this.router.navigate(['/home/admin/editstaff'], {queryParams: {id: staff['personId']}})
  }

  assignService(){
    this.router.navigate(['/home/admin/assignservice'])
  }

 



}

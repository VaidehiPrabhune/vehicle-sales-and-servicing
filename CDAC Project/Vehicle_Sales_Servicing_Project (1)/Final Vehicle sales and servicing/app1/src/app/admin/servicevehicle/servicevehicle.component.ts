import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ServicevehicleService } from '../../servicevehicle.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-servicevehicle',
  templateUrl: './servicevehicle.component.html',
  styleUrls: ['./servicevehicle.component.css']
})
export class ServicevehicleComponent implements OnInit {

  servicevehicles = []
 //status=''

  constructor(private toastr: ToastrService,
    private router: Router,
    private servicevehicleService: ServicevehicleService) { }

  ngOnInit(): void {
    this.loadUsers()
  }

  loadUsers() {
    this.servicevehicleService
      .getVehicles()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.servicevehicles = response['data']
          //console.log(this.servicevehicles)
        } else {
          console.log(response['error'])
        }
      })
  }
 
  toggleActive(servicevehicles) {
    this.servicevehicleService
      .toggleActive(servicevehicles)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.loadUsers()
        } else {
          console.log(response['error'])
        }
      }) 
    }


  onDelete(servicevehicles) {
    this.servicevehicleService
      .deleteCartItem(servicevehicles['serviceId'])
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.toastr.success('deleted Service vehicle')
          this.loadUsers()
        }
      })
  }

}

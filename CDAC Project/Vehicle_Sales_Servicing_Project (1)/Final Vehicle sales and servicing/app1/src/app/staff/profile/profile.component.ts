import { StaffserviceService } from './../../staffservice.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  staffs = []
  schedules=[]
  constructor(
    private router: Router,
    private staffService: StaffserviceService) { }

  ngOnInit(): void {
    this.getStaffProfile()
  }

 

  getStaffProfile() {
   
    this.staffService
      .getStaffProfile(sessionStorage['personId'])
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.staffs = response['data']
          console.log(this.staffs)
        } else {
          console.log(response['error'])
        }
      })
  }

  getStaffSchedule(staff) {
      
    this.staffService
      .getStaffSchedule(staff['personId'])
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.schedules = response['data']
          console.log(this.schedules)
        } else {
          console.log(response['error'])
        }
      })
  }

  placeOrder() {
    this.router.navigate(['/home/staff/placeorder'])
  }

  

}

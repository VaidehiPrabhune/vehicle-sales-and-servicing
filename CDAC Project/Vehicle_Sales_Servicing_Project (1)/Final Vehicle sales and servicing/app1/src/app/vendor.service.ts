import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class VendorService{

  url = 'http://localhost:5000/profile'

  constructor(
    private httpClient: HttpClient) { }

    getVendor() {
      // add the token in the request header
      const httpOptions = {
       headers: new HttpHeaders({
         token: sessionStorage['token'],
         role:sessionStorage['role']
       })
     };
     
     return this.httpClient.get(this.url+'/vendorDetails', httpOptions)
   }

  updateVendor(name: String, contactNo: String, address: String, city: String,state: String, country : String, pincode: String) {
    // add the token in the request header
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token'],
       role:sessionStorage['role']
     })
   };

   
   const body = {
    name: name, 
    contactNo: contactNo, 
    address: address, 
    city: city,
    state: state, 
    country : country, 
    pincode: pincode
   }
   
   return this.httpClient.put(this.url + "/updatePersonalDetails",body, httpOptions)
 }

 getOrder() {
  // add the token in the request header
  const httpOptions = {
   headers: new HttpHeaders({
     token: sessionStorage['token'],
     //role:sessionStorage['role']
   })
 };
 
 return this.httpClient.get(this.url+'/vendorOrderDetails', httpOptions)

}

  updateOrder(partsCost: number, partDeliveryDate, quantity: number, deliveryStatus: number) {
    // add the token in the request header
    const httpOptions = {
    headers: new HttpHeaders({
      token: sessionStorage['token'],
      role:sessionStorage['role']
    })
  };

  
  const body = {
    partsCost: partsCost, 
    partDeliveryDate: partDeliveryDate, 
    quantity: quantity, 
    deliveryStatus: deliveryStatus
   
  }
  
  return this.httpClient.put(this.url+"/updateVendorOrderDetails",body, httpOptions)
}

}


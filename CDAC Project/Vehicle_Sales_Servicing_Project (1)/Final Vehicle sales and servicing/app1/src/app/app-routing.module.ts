
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthService } from './auth/auth.service';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  // default route
  { path: '', redirectTo: '/auth/maindashboard', pathMatch: 'full' },
  { 
    path: 'home',
    component: HomeComponent,
    canActivate : [AuthService],
    children: [
      { path: 'admin', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule ) },
      { path: 'customer', loadChildren: () => import('./customer/customer.module').then(m => m.CustomerModule) },
      { path: 'staff', loadChildren: () => import('./staff/staff.module').then(m => m.StaffModule )},
      { path: 'vendor', loadChildren: () => import('./vendor/vendor.module').then(m => m.VendorModule  ) }
    ]
  },
  { path : 'auth',  loadChildren : () => import('./auth/auth.module').then(m => m.AuthModule)}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

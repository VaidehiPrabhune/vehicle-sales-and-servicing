import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate {
 
    url = 'http://localhost:5000/login'
  
    constructor( 
      private router : Router,
      private httpClient : HttpClient ) { }
  
  
      login(emailId : string, password : string )
      {
        const body = {
          emailId :emailId,
          password :password
        }
        return this.httpClient.post(this.url + '/person_signin', body)
      }
    signup(name: string, contactNo : string, emailId: string, password: String,  address : string, city : string,
      state : string,country : string,pincode : string,role : string)
    {
      const body = {
        name:name,
        contactNo:contactNo,
        emailId: emailId,
        password: password,
        address:address,
        city:city,
        state:state,
        country:country,
        pincode:pincode,
        role:role
                
      }
  
      return this.httpClient.post(this.url + '/person_signup', body)
    }
  
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
       if (sessionStorage['token']) 
       {
       // user is already logged in
        // launch the component
         return true
       }
       // force user to login
       this.router.navigate(['/auth/login'])
       // user has not logged in yet
       // stop launching the component
       return false 
    }
   
  }
 
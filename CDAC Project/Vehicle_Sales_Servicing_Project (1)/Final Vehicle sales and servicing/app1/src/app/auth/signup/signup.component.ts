import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  name: ' '
  contactNo : ' '
  emailId: ''
  password:'' 
  address : ' '
  city : ''
  state : ''
  country :'' 
  pincode : '' 
  role : ' '

  constructor(
    private router :  Router,
    private toastr : ToastrService,
    private authService : AuthService
  ) { }

  ngOnInit(): void {
  }

 
  onSignup()
  {
    if(this.name.length == 0)
    {
      this.toastr.error('Enter your name')
    }
    else if(this.contactNo.length == 0)
    {
      this.toastr.error('Enter your contactNo')
    }
    else if(this.emailId.length == 0)
    {
      this.toastr.error('Enter your emailId')
    }
    else if(this.password.length == 0)
    {
      this.toastr.error('Enter your Password')
    }
    else if(this.address.length == 0)
    {
      this.toastr.error('Enter your address')
    }
    else if(this.city.length == 0)
    {
      this.toastr.error('Enter your city')
    }
    else if(this.state.length == 0)
    {
      this.toastr.error('Enter your state')
    }
    else if(this.country.length == 0)
    {
      this.toastr.error('Enter your country')
    }
    else if(this.pincode.length == 0)
    {
      this.toastr.error('Enter your pincode')
    }
    else
    {
      this.authService.signup(this.name,this.contactNo,this.emailId, this.password,this.address
      ,this.city,this.state,this.country,this.pincode,this.role).subscribe(response => {
        if(response['status'] == 'success')
        {
            this.toastr.success('Registered Successfully')
        }
        else
        {
          console.log(response['error'])
        }
      })
    }
    }

}

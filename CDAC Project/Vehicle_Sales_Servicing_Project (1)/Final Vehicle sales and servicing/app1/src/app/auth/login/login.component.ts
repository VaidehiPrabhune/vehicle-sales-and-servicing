import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  emailId = ''
  password = ''

  constructor(
    private router :  Router,
    private toastr : ToastrService,
    private authService : AuthService) { }

  ngOnInit(): void {
  }

  onLogin()
  {
    if(this.emailId.length == 0)
    {
      this.toastr.error('Enter your Email')
    }
    else if(this.password.length == 0)
    {
      this.toastr.error('Enter your Password')
    }
    else
    {
      this.authService.login(this.emailId, this.password).subscribe(response => {
        if(response['status'] == 'success')
        {
          const data = response['data']
          const name = data['name']
  
          console.log(data);
  
          // cache the user info
          sessionStorage['token'] = data['token']
          sessionStorage['name'] = data['name']
          sessionStorage['role'] = data['role']
          sessionStorage['personId']=data['personId']

          this.toastr.success(`Welcome ${name}`)
          if(sessionStorage['role']=='admin')         
            this.router.navigate(['/home/admin/admindashboard'])

          if(sessionStorage['role']=='customer')  
            this.router.navigate(['home/customer/customerdashboard'])

          if(sessionStorage['role']=='staff')  
            this.router.navigate(['home/staff/staffdashboard'])  

          if(sessionStorage['role']=='vendor')  
            this.router.navigate(['home/vendor/vendordashboard'])  
          
        // this.router.navigate(['./home'])
        }
        else
        {
          this.toastr.error('Invalid Email or Password')
        }
      })
    }
    }
}
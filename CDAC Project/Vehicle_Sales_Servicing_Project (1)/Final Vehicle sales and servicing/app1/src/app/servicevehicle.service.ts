import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ServicevehicleService {

  url = 'http://localhost:5000/serviceVehicle'

  constructor(
    private httpClient: HttpClient) { }
  
  getVehicles() {
     // add the token in the request header
     const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    
    return this.httpClient.get(this.url+'/VehicleServiceDetails', httpOptions)
  }

  
  toggleActive(servicevehicle) {
    // add the token in the request header
    const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    
    // suspend user if already active or activate otherwise
    const body = {
    //  status: servicevehicle['status'] == 0 ? 1 : 0
    }

    return this.httpClient.put(this.url + "/custoUpdateStatus/" + servicevehicle['serviceId'], body, httpOptions)
  }
  deleteCartItem(id) {
    // add the token in the request header
    const httpOptions = {
     headers: new HttpHeaders({
       token: sessionStorage['token']
     })
   };
   
   return this.httpClient.delete(this.url + "/service/" + id, httpOptions)
 }

 updateCartItem(id, status) {
  // add the token in the request header
  const httpOptions = {
   headers: new HttpHeaders({
     token: sessionStorage['token']
   })
 };

 const body = {
   status: status
 }
 
 return this.httpClient.put(this.url + "/custoUpdateStatus/" + id, body, httpOptions)
}

 
}

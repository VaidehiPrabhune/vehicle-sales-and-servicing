import { ServicevehicleService } from './servicevehicle.service';
import { TestBed } from '@angular/core/testing';



describe('ServicevehicleService', () => {
  let service: ServicevehicleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServicevehicleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustomerserviceService } from 'src/app/admin/customerservice.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  persons  = []
  constructor(
    private router :  Router,
    private toastr : ToastrService,
    private customerserviceService : CustomerserviceService
  ) { }


  ngOnInit(): void {
      
    this.customerserviceService
      .getPersonalDetails()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.persons = response['data']
          console.log( this.persons)
        } else {
          console.log(response['error'])
        }
      })
  
    
  }

  onDelete(person) {
    this.customerserviceService
      .deletePerson(person['personId'])
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.toastr.success('Customer removed successfully')
          
        }
      })
  }


}



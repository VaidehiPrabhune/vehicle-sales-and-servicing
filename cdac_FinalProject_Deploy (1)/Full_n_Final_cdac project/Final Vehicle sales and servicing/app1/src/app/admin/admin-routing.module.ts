import { ServicevehicleComponent } from '../admin/servicevehicle/servicevehicle.component';
import { CustomerComponent } from './customer/customer.component';
import { BuyvehicleComponent } from '../admin/buyvehicle/buyvehicle.component';
import { AdmindashboardComponent } from './admindashboard/admindashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { SalevehicleComponent } from '../admin/salevehicle/salevehicle.component';
import { StaffComponent } from './staff/staff.component';
import { VendorComponent } from './vendor/vendor.component';
import { StaffaddComponent } from './staffadd/staffadd.component';
import { AssignserviceComponent } from './assignservice/assignservice.component';
import { EditstaffComponent } from './editstaff/editstaff.component';


const routes: Routes = [
  {path:'admindashboard',component:AdmindashboardComponent},
  {path:'buyvehicle',component:BuyvehicleComponent},
  {path:'customer',component:CustomerComponent},
  {path:'profile',component:ProfileComponent},
  {path:'salevehicle',component:SalevehicleComponent},
  {path:'servicevehicle',component:ServicevehicleComponent},
  {path:'staff',component:StaffComponent},
  {path:'vendor',component:VendorComponent},
  {path:'staffadd',component:StaffaddComponent},
  {path:'assignservice',component:AssignserviceComponent},
  {path:'editstaff',component:EditstaffComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicevehicleComponent } from './servicevehicle.component';

describe('ServicevehicleComponent', () => {
  let component: ServicevehicleComponent;
  let fixture: ComponentFixture<ServicevehicleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServicevehicleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicevehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

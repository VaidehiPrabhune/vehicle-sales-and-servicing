import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { StaffserviceService } from 'src/app/staffservice.service';

@Component({
  selector: 'app-assignservice',
  templateUrl: './assignservice.component.html',
  styleUrls: ['./assignservice.component.css']
})
export class AssignserviceComponent implements OnInit {

  serviceId:0
  personId:0
  constructor(
    private router :  Router,
    private toastr : ToastrService,
    private staffserviceService : StaffserviceService
  ) { }

  ngOnInit(): void {
  }

  

  OnAddService()
  {
    if(this.serviceId.toString().length == 0)
    {
      this.toastr.error('Enter Service Id')
    }
    else if(this.personId.toString().length== 0)
    {
      this.toastr.error('Enter person Id')
    }
    else
    {
      this.staffserviceService.addService(this.serviceId,this.personId).subscribe(response => {
        if(response['status'] == 'success')
        {
            this.toastr.success('Service assigned Successfully')
            this.router.navigate(['/home/admin/staff'])
           
        }
        else
        {
          console.log(response['error'])
        }
      })
    }
    }

   
}

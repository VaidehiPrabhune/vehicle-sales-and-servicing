import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Time } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class CustomerserviceService {

  url = 'http://54.209.3.70:5000/customer'
  
    constructor( 
      private router : Router,
      private httpClient : HttpClient ) { }
  
      insertVehicles(vehName : string, vehModel : string, vehType:String, vehCost,purchaseDate:Date,
      vehNo:string)
      {

        const httpOptions = {
          headers: new HttpHeaders({
            token: sessionStorage['token']
          })
        };

      
         const body = { 
          vehName:vehName,
          vehModel:vehModel,
          vehType:vehType,
          vehCost:vehCost,
          purchaseDate:purchaseDate,         
          vehNo:vehNo
        }
        
        return this.httpClient.post(this.url + '/customerprofile/insertCustomerVehicleDetails', body,httpOptions)
      }


      getPersonalDetails() {

        // add the token in the request header
        const httpOptions = {
         headers: new HttpHeaders({
           token: sessionStorage['token'],
           role:sessionStorage['role']
         })
       };
       
       return this.httpClient.get(this.url + "/customerDetails" ,httpOptions)
      }


      getVehicles() {
        // add the token in the request header
        const httpOptions = {
         headers: new HttpHeaders({
           token: sessionStorage['token'],
           role:sessionStorage['role']
         })
       };
       return this.httpClient.get(this.url + '/buyCustomerVehicle/VehicleDetails', httpOptions)
      }

      getSingleVehicleDetails(personId) {
        // add the token in the request header
        const httpOptions = {
         headers: new HttpHeaders({
           token: sessionStorage['token']
         })
       };
       return this.httpClient.get(this.url + '/buyCustomerVehicle/SingleVehicleDetails/'+ personId, httpOptions)
      }

      insertPaymentDetails(custId:number,vehCost : Number, orderDate : Date, deliveryDate:Date, showroomName:string,paymentType:string,
        totalAmount:number)
        {
  
          const httpOptions = {
            headers: new HttpHeaders({
              token: sessionStorage['token']
            })
          };
  
        
           const body = { 
            vehCost:vehCost,
            orderDate:orderDate,
            deliveryDate:deliveryDate,
            showroomName:showroomName,
            paymentType:paymentType,         
            totalAmount:totalAmount
          }
          
          return this.httpClient.post(this.url + '/buyCustomerVehicle/insertBuyVehicleDetails/'+custId, body,httpOptions)
        }

        insertServceVehicleDetails(custId:number,serviceCost : Number, serviceDate : Date, deliveryDate:Date,serviceTime:Time,
          serviceType:string[] ,costing:number[],paymentType:string,
          totalAmount:number,showroomName:string)
        {
          const httpOptions = {
            headers: new HttpHeaders({
              token: sessionStorage['token']
            })
          };
  
        
           const body = { 
            custId:custId,
            serviceCost:serviceCost,
            serviceDate:serviceDate,
            deliveryDate:deliveryDate,
            serviceTime:serviceTime,
            serviceType:serviceType,
            costing:costing,
            paymentType:paymentType,         
            totalAmount:totalAmount,
            showroomName:showroomName
          }

          return this.httpClient.post(this.url + '/servicing/insertServicingDetails', body,httpOptions)
        }
      
        deletePerson(personId) {
  // add the token in the request header
  const httpOptions = {
   headers: new HttpHeaders({
     token: sessionStorage['token']
   })
 };

 console.log(personId)
 
 return this.httpClient.delete(this.url + "/deleteCustomerDetails/" + personId, httpOptions)
}



addPerson(name: string, contactNo : number, emailId: string, password: String,  address : string, city : string,
  state : string,country : string,pincode : number,role : string)
{
  const body = {
    name:name,
    contactNo:contactNo,
    emailId: emailId,
    password: password,
    address:address,
    city:city,
    state:state,
    country:country,
    pincode:pincode,
    role:role
            
  }

  return this.httpClient.post(this.url + '/addstaff', body)
}





updatestaff(id, name: string, contactNo: number,  address:string,city:string,state:string,country:string,pincode:number) {
  // add the token in the request header
  const httpOptions = {
   headers: new HttpHeaders({
     token: sessionStorage['token']
   })
 };

 const body = {
  name:name,
  contactNo:contactNo,  
  address:address,
  city:city,
  state:state,
  country:country,
  pincode:pincode
  
 }
 
 return this.httpClient.put(this.url + "/updateStaffDetails/" + id, body, httpOptions)
}


  }
      


import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class VendorService{

  url = 'http://54.209.3.70:5000/vendor'

  constructor(
    private httpClient: HttpClient) { }

    getVendor() {
      // add the token in the request header
      const httpOptions = {
       headers: new HttpHeaders({
         token: sessionStorage['token'],
         role:sessionStorage['role']
       })
     };
     
     return this.httpClient.get(this.url+'/vendorDetails', httpOptions)
   }


}


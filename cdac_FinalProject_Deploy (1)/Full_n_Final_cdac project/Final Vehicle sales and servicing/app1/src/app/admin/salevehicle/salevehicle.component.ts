import { SalevehicleService } from '../../salevehicle.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-salevehicle',
  templateUrl: './salevehicle.component.html',
  styleUrls: ['./salevehicle.component.css']
})
export class SalevehicleComponent implements OnInit {
  salevehicles = []

  constructor(
    private router: Router,
    private salevehicleService: SalevehicleService) { }

  ngOnInit(): void {
    this.loadUsers()
  }

  loadUsers() {
    this.salevehicleService
      .getVehicles()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.salevehicles = response['data']
        } else {
          console.log(response['error'])
        }
      })
  }
}


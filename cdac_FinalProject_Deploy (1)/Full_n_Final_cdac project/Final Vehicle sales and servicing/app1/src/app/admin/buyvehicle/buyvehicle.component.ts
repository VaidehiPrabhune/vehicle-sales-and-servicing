import { BuyvehicleService } from '../../buyvehicle.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-buyvehicle',
  templateUrl: './buyvehicle.component.html',
  styleUrls: ['./buyvehicle.component.css']
})
export class BuyvehicleComponent implements OnInit {
  buyvehicles = []

  constructor(
    private router: Router,
    private buyvehicleService: BuyvehicleService) { }

  ngOnInit(): void {
    this.loadUsers()
  }

  loadUsers() {
    this.buyvehicleService
      .getVehicles()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.buyvehicles = response['data']
          console.log(this.buyvehicles)
        } else {
          console.log(response['error'])
        }
      })
  }
}



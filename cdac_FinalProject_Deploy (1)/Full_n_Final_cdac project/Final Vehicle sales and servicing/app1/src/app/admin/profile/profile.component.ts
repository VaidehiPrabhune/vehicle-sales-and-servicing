import { AdminService } from '../../admin.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  
  admins = []

  constructor(
    private router: Router,
    private adminService: AdminService) { }

  ngOnInit(): void {
    this.loadProducts()
  }

  loadProducts() {
    this.adminService
      .getProducts()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.admins = response['data']
        } else {
          console.log(response['error'])
        }
      })
  }

}

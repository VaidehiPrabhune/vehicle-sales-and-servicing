import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StaffserviceService } from 'src/app/staffservice.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-placeorder',
  templateUrl: './placeorder.component.html',
  styleUrls: ['./placeorder.component.css']
})
export class PlaceorderComponent implements OnInit {

  partName=''
  company=''
  qty=0
  personId=0


  constructor(
    
    private toastr: ToastrService,
    private router: Router,
    private staffService: StaffserviceService) { }

  ngOnInit(): void {
    //this.placeOrder()
  }

  placeOrder(){
    console.log(this.partName)
    this.staffService
    .placeOrder(this.partName,this.qty,this.personId)
    .subscribe(response => {
      if (response['status'] == 'success') {
        this.toastr.success(`order place successfully`)
        this.router.navigate(['/home/staff/profile'])
      }
    })
  }

}

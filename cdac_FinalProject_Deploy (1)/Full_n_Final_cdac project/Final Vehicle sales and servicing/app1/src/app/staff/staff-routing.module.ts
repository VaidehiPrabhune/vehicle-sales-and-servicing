import { PlaceorderComponent } from './placeorder/placeorder.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StaffdashboardComponent } from './staffdashboard/staffdashboard.component';
import { ProfileComponent } from '../staff/profile/profile.component';
import { ScheduleComponent } from './schedule/schedule.component';




const routes: Routes = [
  {path:'staffdashboard',component:StaffdashboardComponent},
  {path:'profile',component:ProfileComponent},
  {path:'schedule',component:ScheduleComponent},
  {path:'placeorder',component:PlaceorderComponent}
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaffRoutingModule { }

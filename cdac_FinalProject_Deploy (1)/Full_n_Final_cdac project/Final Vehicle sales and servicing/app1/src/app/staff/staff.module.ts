import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StaffRoutingModule } from './staff-routing.module';

import { FormsModule } from '@angular/forms';
import { StaffdashboardComponent } from './staffdashboard/staffdashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { PlaceorderComponent } from './placeorder/placeorder.component';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';



@NgModule({
  declarations: [StaffdashboardComponent, ProfileComponent, ScheduleComponent, PlaceorderComponent],
  imports: [
    CommonModule,
    StaffRoutingModule,
    FormsModule,
    HttpClientModule ,// required animations module
    ToastrModule.forRoot(),
  ]
})
export class StaffModule { }

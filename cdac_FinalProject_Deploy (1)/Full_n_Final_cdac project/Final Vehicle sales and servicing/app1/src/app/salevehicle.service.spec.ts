import { TestBed } from '@angular/core/testing';

import { SalevehicleService } from './salevehicle.service';

describe('SalevehicleService', () => {
  let service: SalevehicleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalevehicleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

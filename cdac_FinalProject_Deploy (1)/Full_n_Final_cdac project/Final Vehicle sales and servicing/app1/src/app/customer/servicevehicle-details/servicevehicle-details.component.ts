import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustomerserviceService } from '../customerservice.service';

@Component({
  selector: 'app-servicevehicle-details',
  templateUrl: './servicevehicle-details.component.html',
  styleUrls: ['./servicevehicle-details.component.css']
})
export class ServicevehicleDetailsComponent implements OnInit {

  personId: 0
  vehName: ' '
  vehModel : ' '
  vehType: ''
  vehCost:'' 
  purchaseDate : Date
  status : ''
  image :null
  vehNo:''

  
  persons  = []
  customer=null;

  constructor(
    private router :  Router,
    private toastr : ToastrService,
    private customerserviceService : CustomerserviceService
  ) { }

  ngOnInit(): void {
    this.customerserviceService
      .getPersonalDetails()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.persons = response['data']
          this.customer=this.persons[0]
        } else {
          console.log(response['error'])
        }
      })


  }

  onImageSelect(event) {
    this.image = event.target.files[0]
  }

  onInsertVehicle()
  {
    
      this.customerserviceService.insertVehicles(this.vehName,this.vehModel,this.vehType,
      this.vehCost,this.purchaseDate,this.vehNo).subscribe(response => {
        if(response['status'] == 'success')
        {        
            this.router.navigate(['/home/customer/servicevehicle-imageupload'])
        }
        else
        {
          console.log(response['error'])
        }
      })
    
  }

  onCancel()
  {
    this.router.navigate(['/home/customer/customerdashboard'])
  }


}

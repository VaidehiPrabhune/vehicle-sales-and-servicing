import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicevehicleDetailsComponent } from './servicevehicle-details.component';

describe('ServicevehicleDetailsComponent', () => {
  let component: ServicevehicleDetailsComponent;
  let fixture: ComponentFixture<ServicevehicleDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServicevehicleDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicevehicleDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

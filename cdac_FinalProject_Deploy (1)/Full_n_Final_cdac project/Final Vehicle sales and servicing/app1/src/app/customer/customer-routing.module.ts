import { ServicevehicleImageuploadComponent } from './servicevehicle-imageupload/servicevehicle-imageupload.component';
import { ServicevehicleDetailsComponent } from './servicevehicle-details/servicevehicle-details.component';

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerdashboardComponent } from './customerdashboard/customerdashboard.component';

import { ProfileComponent } from './profile/profile.component';
import { ServicevehicleComponent } from './servicevehicle/servicevehicle.component';
import { SalevehicleComponent } from './salevehicle/salevehicle.component';
import { VehicleAlldetailsComponent } from './vehicle-alldetails/vehicle-alldetails.component';
import { BuyvehicleComponent } from './buyvehicle/buyvehicle.component';
import { VehicleUploadImageComponent } from './vehicle-upload-image/vehicle-upload-image.component';
import { PaymentDetailsComponent } from './payment-details/payment-details.component';





const routes: Routes = [
  {path:'customerdashboard',component:CustomerdashboardComponent},
  {path:'buyvehicle',component:BuyvehicleComponent},
  {path:'profile',component:ProfileComponent},
  {path:'salevehicle',component:SalevehicleComponent},
  {path:'servicevehicle',component:ServicevehicleComponent},
  {path:'vehicle-alldetails',component:VehicleAlldetailsComponent},
  {path:'vehicle-upload-image',component:VehicleUploadImageComponent},
  {path:'payment-details',component:PaymentDetailsComponent},
  {path:'servicevehicle-details',component:ServicevehicleDetailsComponent},
  {path:'servicevehicle-imageupload',component:ServicevehicleImageuploadComponent},
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }

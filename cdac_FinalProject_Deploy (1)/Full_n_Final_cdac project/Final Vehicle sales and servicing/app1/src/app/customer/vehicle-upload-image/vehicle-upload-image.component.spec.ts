import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleUploadImageComponent } from './vehicle-upload-image.component';

describe('VehicleUploadImageComponent', () => {
  let component: VehicleUploadImageComponent;
  let fixture: ComponentFixture<VehicleUploadImageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehicleUploadImageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleUploadImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

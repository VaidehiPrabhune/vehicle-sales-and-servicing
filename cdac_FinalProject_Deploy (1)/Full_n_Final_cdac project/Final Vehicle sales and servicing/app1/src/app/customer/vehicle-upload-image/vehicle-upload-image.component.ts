import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomerserviceService } from '../customerservice.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-vehicle-upload-image',
  templateUrl: './vehicle-upload-image.component.html',
  styleUrls: ['./vehicle-upload-image.component.css']
})
export class VehicleUploadImageComponent implements OnInit {

  selectedFile = null
  
  constructor(
    private router: Router,
    private toastr : ToastrService,
    private activatedRoute: ActivatedRoute,
    private customerservice: CustomerserviceService) { }

  ngOnInit(): void {
  }

  onImageSelect(event) {
    this.selectedFile = event.target.files[0]
  }

  onUploadImage() {
    this.customerservice
      .uploadImage(this.selectedFile)
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.toastr.success(`Vehicle details added successfully.`)
          this.toastr.success(`Thanks ${sessionStorage['name']}. Visit Again !!`)
          this.router.navigate(['/home/customer/customerdashboard'])
        } else {
          console.log(response['error'])
        }
      })
  }

}

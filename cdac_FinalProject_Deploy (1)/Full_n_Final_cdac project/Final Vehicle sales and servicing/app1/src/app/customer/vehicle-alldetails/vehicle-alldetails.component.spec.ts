import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleAlldetailsComponent } from './vehicle-alldetails.component';

describe('VehicleAlldetailsComponent', () => {
  let component: VehicleAlldetailsComponent;
  let fixture: ComponentFixture<VehicleAlldetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VehicleAlldetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleAlldetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

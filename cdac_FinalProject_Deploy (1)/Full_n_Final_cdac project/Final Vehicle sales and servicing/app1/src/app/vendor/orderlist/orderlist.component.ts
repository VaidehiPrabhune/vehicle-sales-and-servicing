import { VendorService } from '../../vendor.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-orderlist',
  templateUrl: './orderlist.component.html',
  styleUrls: ['./orderlist.component.css']
})
export class OrderlistComponent implements OnInit {

  vendors = []

  constructor(
    private router: Router,
    private vendorService: VendorService) { }

  ngOnInit(): void {
    this.loadOrders()
  }

  loadOrders() {
    this.vendorService
      .getOrder()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.vendors = response['data']
        } else {
          console.log(response['error'])
        }
      })
  }

  onEdit(vendor) {
    console.log(vendor)
    this.router.navigate(['/home/vendor/update-order'], {queryParams: {personId: vendor['personId']}})
  }
}

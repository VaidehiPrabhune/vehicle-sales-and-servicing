import { VendorService } from '../../vendor.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  vendors = []

  constructor(
    private router: Router,
    private vendorService: VendorService) { }

  ngOnInit(): void {
    this.loadVendors()
  }

  loadVendors() {
    this.vendorService
      .getVendor()
      .subscribe(response => {
        if (response['status'] == 'success') {
          this.vendors = response['data']
        } else {
          console.log(response['error'])
        }
      })
  }

  onEdit(vendor) {
    this.router.navigate(['/home/vendor/add-person'], {queryParams: {personId: vendor['personId']}})
  }

}


import { VendorService } from '../../vendor.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-add-person',
  templateUrl: './add-person.component.html',
  styleUrls: ['./add-person.component.css']
})
export class AddPersonComponent implements OnInit {


  name = ''
  contactNo = ''
  address = ''
  city = ''
  state = ''
  country = ''
  pincode = ''

  vendor = null

  constructor(
    private router: Router,
    private vendorService: VendorService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {

    const personId = this.activatedRoute.snapshot.queryParams['personId']
    if (personId) {
      // edit vendor
      this.vendorService
        .getVendor()
        .subscribe(response => {
          if (response['status'] == 'success') {
            const vendors = response['data']
            if (vendors.length > 0) {
              this.vendor = vendors[0]
              this.name = this.vendor['name']
              this.contactNo = this.vendor['contactNo']
              this.address = this.vendor['address']
              this.city = this.vendor['city']
              this.state = this.vendor['state']
              this.country = this.vendor['country']
              this.pincode = this.vendor['pincode']
            }
          }
        })
      }
    }

    onUpdate() {

      if (this.vendor) {
        // edit
        console.log(this.vendor);
        this.vendorService
          .updateVendor(this.name, this.contactNo, this.address, this.city, this.state, this.country, this.pincode)
          .subscribe(response => {
            if (response['status'] == 'success') {
              this.router.navigate(['/home/vendor/profile'])
            }
          })
      }
    }
}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BuyvehicleService {

  url = 'http://54.209.3.70:5000/BuyVehicle'

  constructor(
    private httpClient: HttpClient) { }
  
  getVehicles() {
     // add the token in the request header
     const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token']
      })
    };
    
    return this.httpClient.get(this.url + '/BuyVehicleDetails', httpOptions)
  }
}



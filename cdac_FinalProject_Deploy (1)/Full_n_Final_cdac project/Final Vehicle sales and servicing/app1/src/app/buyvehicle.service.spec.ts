import { TestBed } from '@angular/core/testing';

import { BuyvehicleService } from './buyvehicle.service';

describe('BuyvehicleService', () => {
  let service: BuyvehicleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BuyvehicleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

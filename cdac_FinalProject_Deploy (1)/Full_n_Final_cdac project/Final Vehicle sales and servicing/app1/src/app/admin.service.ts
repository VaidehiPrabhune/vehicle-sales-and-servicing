import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  url = 'http://54.209.3.70:5000/admin'

  constructor(
    private httpClient: HttpClient) { }
  
  getProducts() {
     // add the token in the request header
     const httpOptions = {
      headers: new HttpHeaders({
        token: sessionStorage['token'],
        role:sessionStorage['role']
      })
    };
    
    return this.httpClient.get(this.url+'/adminDetails', httpOptions)
  }
}

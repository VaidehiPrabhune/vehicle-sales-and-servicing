const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const router = express.Router()
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const config = require('../../config')

//--------------------------GET(admin details)
router.get('/adminDetails', (request, response) => {
    const statement = `select ad.adminId,pd.personId,pd.name,pd.contactNo,pd.emailId,
  pd.address from personal_details pd INNER JOIN  admin_details ad
    ON pd.personId=ad.personId where pd.role='admin'`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
    })

//-----------------------admin signin------------------
  router.post('/person_signin', (request, response) => {
    const { emailId, password } = request.body
    const statement = `select personId,name,emailId from personal_details 
                      where emailId = '${emailId}' and password = '${crypto.SHA256(password)}' and role='admin'`
    db.query(statement, (error, users) => {
      if (error) {
        response.send({ status: 'error', error: error })
      } else {
        if (users.length == 0) {
          // user does not exist
          response.send({ status: 'error', error: 'user does not exist' })
        } else {
          // user exists
          const user = users[0]
          const token = jwt.sign({id: user['personId']},config.secret)
          response.send(utils.createResult(error,
            {
              name: user['name'],
              emailId: user['emailId'],
              token: token
            }
          ))
          
        }
      }
    })
  }) 









module.exports = router
const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const router = express.Router()
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const config = require('../../config')


//------------------------customer Details GET---------------------------------------------

router.get('/customerDetails', (request, response) => { 
    const statement = `select pd.personId,pd.name,pd.contactNo,pd.emailId,
    pd.address,pd.city,pd.state,pd.country,pd.pincode
    from personal_details pd INNER JOIN  customer_vehicle_details cd
    ON pd.personId=cd.personId where pd.role='customer'`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })

})

//-----------------------update customer vehicle details----------------------------------------  
router.put('/updateCustomerVehicleDetails/:id', (request, response) => {
    const token = request.headers['token']
  
    try {
      const data = jwt.verify(token, config.secret)
      const { id } = request.params
      const { vehName,vehModel,vehType,vehCost,purchaseDate,status,image,vehNo } = request.body
      const statement = `update customer_vehicle_details set personId='${request.userId}',vehName='${vehName}'
      ,vehModel='${vehModel}',vehType='${vehType}',vehCost='${vehCost}',purchaseDate='${purchaseDate}',status='${status}',
      image='${image}',vehNo='${vehNo}' where custId='${id}'`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
  
    } catch (ex) {
      response.status(401)
      response.send('you are not allowed to access this API')
    }
  })


//------------------------DELETE customer details-----------------------------------------
router.delete('/deleteCustomerDetails/:custId', (request, response) => {
    const token = request.headers['token']
    try {
  
      const data = jwt.verify(token, config.secret)
      const { custId } = request.params
      const statement = `delete from customer_vehicle_details where custId = '${custId}'`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
  
    } catch (ex) {
      response.status(401)
      response.send('you are not allowed to access this API')
    }
  })

//-----------------------update customer vehicle details----------------------------------------  
router.put('/updateCustomerVehicleDetails/:id', (request, response) => {
    const token = request.headers['token']
  
    try {
      const data = jwt.verify(token, config.secret)
      const { id } = request.params
      const { vehName,vehModel,vehType,vehCost,purchaseDate,status,image,vehNo } = request.body
      const statement = `update customer_vehicle_details set personId='${request.userId}',vehName='${vehName}'
      ,vehModel='${vehModel}',vehType='${vehType}',vehCost='${vehCost}',purchaseDate='${purchaseDate}',status='${status}',
      image='${image}',vehNo='${vehNo}' where custId='${id}'`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error, data))
      })
  
    } catch (ex) {
      response.status(401)
      response.send('you are not allowed to access this API')
    }
  })










module.exports = router
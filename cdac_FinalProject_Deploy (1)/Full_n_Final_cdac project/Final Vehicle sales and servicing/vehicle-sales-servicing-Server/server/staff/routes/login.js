const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const router = express.Router()
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const config = require('../../config')
                


 // -----------------------------admin login CRUD---------------------------------------- 
//----------------------------------------- Read-----------------------------------

/*  select p.personId,p.name,p.contactNo,p.address,p.city,p.state,p.country,p.pincode,s.staffId,
          sr.serviceId from personal_details p inner join staff_details s on p.personId=s.personId inner join 
          servicing_details sr on s.serviceId=sr.serviceId where role='staff' */

  router.get('/allStaffDetails', (request, response) => { 
    const token = request.headers['token']
    try {
          const statement = `select personId,name,contactNo,address,city,state,country,pincode from personal_details where role='staff'`
        db.query(statement, (error, data) => {
          response.send(utils.createResult(error, data))
        })
      }catch (ex) {
        response.status(401)
        response.send('you are not allowed to access this API')
      }
     
    })


    //---------------------------create ---------------------------

    router.post('/addstaff', (request, response) => {
      const {name,contactNo,emailId,password,address,city,state,country,pincode,role} = request.body
      
      console.log(role)
      // encrypt user's password
      const statement = `insert into personal_details(name,contactNo,emailId
        ,password,address,city,state,country,pincode,role) values (
        '${name}','${contactNo}', '${emailId}','${crypto.SHA256(password)}','${address}'
        ,'${city}','${state}','${country}','${pincode}','${role}'
      )`
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error,data))
      }) 
    
    })
  

    


    router.post('/addservice', (request, response) => {
      const {serviceId,personId} = request.body
     
    
      console.log('test')
      // encrypt user's password
      const statement = `insert into staff_details (serviceId,personId) values (${serviceId},${personId})`
      console.log(statement)
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error,data))
      }) 
    
    
     
    })
    


    //-----------------------update staff details----------------------------------------
  
router.put('/updateStaffDetails/:id', (request, response) => {
  const token = request.headers['token']

  try {
    const data = jwt.verify(token, config.secret)
    const { id } = request.params
    const {name,contactNo,address,city,state,country,pincode} = request.body
    const statement = `update personal_details set name='${name}',
    contactNo=${contactNo},   
    address='${address}', 
    city='${city}',
    state='${state}',
    country='${country}'      
    where personId = ${id}`
    console.log(statement)
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })

    

  } catch (ex) {
    response.status(401)
    response.send('you are not allowed to access this API')
  }
})

//------------------------DELETE staff details------------------------------------------

router.delete('/deleteStaffDetails/:id', (request, response) => {
  const token = request.headers['token']
  try {    
    const { id } = request.params
    //consolg.log('id=',id)
    const statement = `delete from personal_details  where personId = '${id}'`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })

  } catch (ex) {
    response.status(401)
    response.send('you are not allowed to access this API')
  }
})


//-------------------------allocate service to staff details-----------------------------------
router.post('/staffServiceAllocation', (request, response) => {
  
  // encrypt user's password
  const {staffId,serviceId,personId} = request.body
  
  const statement = `insert into staff_details(staffId,serviceId,personId) values (
    '${staffId}','${serviceId}','${personId}')`
  console.log(statement)
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error,data))
  }) 

}) 
    //----------------------------------------  crud end-----------------------------------------






    //----------------------------------------staff login - Get staff profile-----------------------------------
    router.get('/staffprofile/:id', (request, response) => { 
      const token = request.headers['token']
      const { id } = request.params
      try {
            const statement = `select p.personId,p.name,p.contactNo,p.address,p.city,p.state,p.country,p.pincode,s.staffId
            from personal_details p inner join staff_details s on p.personId=s.personId where p.personId = '${id}'`
          db.query(statement, (error, data) => {
            response.send(utils.createResult(error, data))
          })
        }catch (ex) {
          response.status(401)
          response.send('you are not allowed to access this API')
        }
       
      })
  
      //----------------------------------------staff login---update staff profile-------------------------
     
    router.put('/updateStaffDetails/:id', (request, response) => {
      const token = request.headers['token']
    
      try {
        const data = jwt.verify(token, config.secret)
        const { id } = request.params
        const { name,contactNo,address} = request.body
        const statement = `update staff_details set name='${name}',contactNo='${contactNo}'
        ,address='${address}' where staffId = '${id}'`
        db.query(statement, (error, data) => {
          response.send(utils.createResult(error, data))
        })
    
      } catch (ex) {
        response.status(401)
        response.send('you are not allowed to access this API')
      }
    })

    //----------------------------------------staff login - Get staff schedule with staff Id-----------------------------------

  router.get('/staffschedule/:id', (request, response) => { 
    const token = request.headers['token']
    const { id } = request.params
    console.log(id)
    try {
          const statement = `select p.name,s.staffId,sr.serviceId,sr.serviceDate,sr.deliveryDate,
          c.vehNo from personal_details p inner join staff_details s on p.personId=s.personId inner join 
          servicing_details sr on s.serviceId=sr.serviceId inner join customer_vehicle_details c on 
          sr.custId=c.custId  where p.personId = '${id}'`
        db.query(statement, (error, data) => {
          response.send(utils.createResult(error, data))
        })
      }catch (ex) {
        response.status(401)
        response.send('you are not allowed to access this API')
      }
     
    })
    


//---------------------place part order to vendor----------------------------
    router.post('/placeOrder', (request, response) => {
    
      const token = request.headers['token']
    
      try {
      // encrypt user's password
      const {partName,qty,personId} = request.body
      console.log(partName)    
      console.log(qty)
      
      const statement = `insert into vendor_details(partsName,quantity,personId) values (
        '${partName}','${qty}','${personId}' )`
      console.log(statement)
      db.query(statement, (error, data) => {
        response.send(utils.createResult(error,data))
      }) 
    } catch (ex) {
      response.status(401)
      response.send('you are not allowed to access this API')
    }
    
    })

    


  module.exports = router
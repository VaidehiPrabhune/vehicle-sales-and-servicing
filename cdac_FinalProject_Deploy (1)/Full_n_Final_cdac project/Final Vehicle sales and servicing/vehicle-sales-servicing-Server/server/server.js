const express = require('express')
const bodyParser = require('body-parser')
const config = require('./config')
const jwt = require('jsonwebtoken')
const cors=require('cors')

//get admin routers
const adminRouter = require('./admin/routes/login')
const vendorRouter = require('./admin/routes/vendor')
const staffRouter = require('./admin/routes/staff')
const customerRouter = require('./admin/routes/customer')
const customerSalesRouter = require('./admin/routes/saleVehicle')
const customerServiceRouter = require('./admin/routes/serviceVehicle')
const customerBuyRouter = require('./admin/routes/buyVehicle')


// get the customer routers
const routerCustomerLogin = require('./customer/routes/login')
const routerCustomerProfile = require('./customer/routes/customerprofile')
const routerCustomerServicing=require('./customer/routes/servicing')
const routerCustomerBuyVehicle=require('./customer/routes/buyCustomerVehicle')
const routerCustomerSales=require('./customer/routes/sales')

//get the vendor routers
const routerVendorLogin = require('./vendor/routes/login')
const routerVendorProfile = require('./vendor/routes/profile')

//get the staff routers
const routerStaffLogin = require('./staff/routes/login')



const app = express()
app.use(cors())
app.use(bodyParser.json())


function getPersonId(request, response, next) {

  if (request.url == '/login/person_signup' 
      || request.url == '/login/person_signin'
      || request.url.startsWith('/buyCustomerVehicle/image/')
      || request.url == '/login/addstaff'
      || request.url == '/login/addservice'
      || request.url == '/login/placeOrder'
       ) {
    // do not check for token 
    next()
  } else {

    try {
      const token = request.headers['token']
      //console.log(token)
      const data = jwt.verify(token, config.secret)
      
      // add a new key named userId with logged in user's id
      request.userId = data['id']
      console.log(request.userId)

      // go to the actual route
      next()
      
    } catch (ex) {
      response.status(401)
      response.send({status: 'error', error: 'protected api'})
    }
  }
}

app.use(getPersonId)
// required to send the static files in the directory named images
app.use(express.static('images/'))


//add the admin routers
app.use('/admin',adminRouter)
app.use('/BuyVehicle',customerBuyRouter)
app.use('/customer',customerRouter)
app.use('/saleVehicle',customerSalesRouter)
app.use('/serviceVehicle',customerServiceRouter)
app.use('/staff',staffRouter)
app.use('/vendor',vendorRouter)


// add the customer routers
app.use('/login', routerCustomerLogin)
app.use('/customerprofile',routerCustomerProfile)
app.use('/servicing',routerCustomerServicing)
app.use('/buyCustomerVehicle',routerCustomerBuyVehicle)
app.use('/sales',routerCustomerSales)

//add vendor routers
app.use('/login',routerVendorLogin)
app.use('/profile',routerVendorProfile)

//add staff routers
app.use('/login',routerStaffLogin)

// default handler
 app.get('/', (request, response) => {
  response.send('welcome to backend')
 })

// starting the server
app.listen(5000, '0.0.0.0', () => {
  console.log('server started on port 5000')
})
const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const router = express.Router()
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const config = require('../../config')

                  

// --------------------POST-------------------

router.post('/vendor_signup', (request, response) => {
    const {name,contactNo,emailId,password,address,city,state,country,pincode,role,isActive} = request.body
    
    // encrypt user's password
    const statement = `insert into personal_details(name,contactNo,emailId
      ,password,address,city,state,country,pincode,role,isActive) values (
      '${name}','${contactNo}', '${emailId}','${crypto.SHA256(password)}','${address}'
      ,'${city}','${state}','${country}','${pincode}','${role}','${isActive}'
    )`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error,data))
    }) 
  
  })






 router.post('/vendor_signin', (request, response) => {
  const { emailId, password } = request.body
  const statement = `select personId,name,emailId from personal_details 
                    where emailId = '${emailId}' and password = '${crypto.SHA256(password)}' where role='vendor'`
  db.query(statement, (error, users) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      if (users.length == 0) {
        // user does not exist
        response.send({ status: 'error', error: 'user does not exist' })
      } else {
        // user exists
        const user = users[0]
        const token = jwt.sign({id: user['personId']},config.secret)
        response.send(utils.createResult(error,
          {
            name: user['name'],
            emailId: user['emailId'],
            token: token
          }
        ))
        
      }
    }
  })
})



module.exports = router
const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const router = express.Router()
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const config = require('../../config')
const fs = require('fs')

//Customer will insert only its personal details not vehicle details 
//so in cusstomer table all vehicle details will be null for customer who wants to buy vehicle.

//-------------------------get image-------------------
router.get('/image/:filename', (request, response) => {
  const {filename} = request.params
  const file = fs.readFileSync(__dirname + '/../../images/' + filename)
  response.send(file)
})

//------------------------(dispaly list of unsold vehicles )BuyVehicle GET---------------------------------------------
router.get('/VehicleDetails', (request, response) => { 
  const statement = `select personId,vehName,vehModel,vehType,vehCost,
  purchaseDate,status,vehNo,image from customer_vehicle_details where status='notsold'`
db.query(statement, (error, data) => {
  response.send(utils.createResult(error, data))
})

})

//------------------------(dispaly details of one unsold vehicle )BuyVehicle GET---------------------------------------------
router.get('/SingleVehicleDetails/:personId', (request, response) => {
  const { personId } = request.params
  const statement = `select custId,vehName,vehModel,vehType,vehCost,
  purchaseDate,vehNo,image from customer_vehicle_details where status='notsold' and personId='${personId}'`
db.query(statement, (error, data) => {
  response.send(utils.createResult(error, data))
})

})

//------------------------(enter buyvehicle details)BuyVehicle POST---------
router.post('/insertBuyVehicleDetails/:custId', (request, response) => {
  const {vehCost,orderDate,deliveryDate,showroomName,paymentType,totalAmount} = request.body
  const { custId } = request.params
  const statement = `insert into buy_details(custId,vehCost,orderDate,deliveryDate,
                      showroomName,paymentType,totalAmount)
                      values ('${custId}','${vehCost}','${orderDate}','${deliveryDate}','${showroomName}','${paymentType}'
                      ,'${totalAmount}'
                      )`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error,data))
  }) 

})


//-------------------update vehicle details
router.put('/updateCustomerVehicleDetails/:id', (request, response) => {
  const token = request.headers['token']
  
  try {
  const data = jwt.verify(token, config.secret)
  const { id } = request.params
  const statement = `update customer_vehicle_details set status='sold' where custId='${id}'`
  db.query(statement, (error, data) => {
  response.send(utils.createResult(error, data))
  })
  
  } catch (ex) {
  response.status(401)
  response.send('you are not allowed to access this API')
  }
  }) 


module.exports = router
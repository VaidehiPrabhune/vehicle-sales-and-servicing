const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const router = express.Router()
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const config = require('../../config')

//------------------------(display vehicle and customer details)servicing GET---------------------------------------------

router.get('/customerDetails', (request, response) => { 
    const statement = `select cd.custId,pd.personId,pd.name,pd.contactNo,pd.emailId,
    pd.password,pd.address,pd.city,pd.state,pd.country,pd.pincode,pd.role,pd.isActive,cd.vehName,cd.vehModel,cd.vehType,cd.vehCost,
    cd.purchaseDate,cd.status,cd.vehNo 
    from personal_details pd INNER JOIN  customer_details cd
    ON pd.personId=cd.personId where pd.role='customer' and cd.personId='${request.userId}'`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
  
  })
  
//------------------------(insert servicing details)servicing POST---------------------------------------------
router.post('/insertServicingDetails', (request, response) => {
  const {custId,serviceCost,serviceDate,deliveryDate,serviceTime,serviceType,costing,paymentType,paymentStatus,totalAmount,showroomName} = request.body
  const statement = `insert into servicing_details(custId,serviceCost,serviceDate,deliveryDate,
                      serviceTime,serviceType,costing,paymentType,paymentStatus,totalAmount,showroomName)
                      values ('${custId}','${serviceCost}','${serviceDate}','${deliveryDate}',
                      '${serviceTime}','${serviceType}','${costing}','${paymentType}','${paymentStatus}',
                      '${totalAmount}','${showroomName}'
                      )`
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error,data))
  }) 

})

//------------------------(update servicing details)servicing PUT---------------------------------------------
router.put('/updateServicingDetails/:custid', (request, response) => {
  const token = request.headers['token']

  try {
    const data = jwt.verify(token, config.secret)
    const { custid } = request.params
    const { serviceCost,serviceDate,deliveryDate,serviceTime,status,serviceType,costing,paymentType,paymentStatus,totalAmount,showroomName} = request.body
    const statement = `update servicing_details set serviceCost='${serviceCost}',serviceDate='${serviceDate}',deliveryDate='${deliveryDate}'
    ,serviceTime='${serviceTime}',status='${status}',serviceType='${serviceType}',costing='${costing}',paymentType='${paymentType}'
    ,paymentStatus='${paymentStatus}',totalAmount='${totalAmount}',showroomName'='${showroomName}'
     where custId = '${custid}'`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })

  } catch (ex) {
    response.status(401)
    response.send('you are not allowed to access this API')
  }
})

















module.exports = router
const express = require('express')
const db = require('../../db')
const utils = require('../../utils')
const router = express.Router()
const crypto = require('crypto-js')
const jwt = require('jsonwebtoken')
const config = require('../../config')

                  

// --------------------personal details signup POST-------------------


router.post('/person_signup', (request, response) => {
    const {name,contactNo,emailId,password,address,city,state,country,pincode,role} = request.body
    
    // encrypt user's password
    const statement = `insert into personal_details(name,contactNo,emailId
      ,password,address,city,state,country,pincode,role) values (
      '${name}','${contactNo}', '${emailId}','${crypto.SHA256(password)}','${address}'
      ,'${city}','${state}','${country}','${pincode}','${role}'
    )`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error,data))
    }) 
  
  })

/*
  router.post('/signin', (request, response) => {
    const { emailId, password } = request.body
    const statement = `select personId,name,emailId from personal_details 
                      where emailId = '${emailId}' and password = '${crypto.SHA256(password)}'`
    db.query(statement, (error, users) => {
      if (error) {
        response.send({ status: 'error', error: error })
      } else {
        if (users.length == 0) {
          // user does not exist
          response.send({ status: 'error', error: 'user does not exist' })
        } else {
          // user exists
          const user = users[0]
          const token = jwt.sign({id: user['personId']},config.secret)
          response.send({ status: 'success', data: {
              name: user['name'],
              emailId: user['emailId'],
              token: token
            }
          })
        }
      }
    })
  })
  */

  // --------------------personal details signin POST-------------------
 router.post('/person_signin', (request, response) => {
  const { emailId, password } = request.body
  let statement = `select personId,name,emailId,role from personal_details 
                    where emailId = '${emailId}' and password = '${crypto.SHA256(password)}'`
  db.query(statement, (error, users) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      if (users.length == 0) {
        // user does not exist
        response.send({ status: 'error', error: 'user does not exist' })
      } else {
        // user exists
        const user = users[0]
        const token = jwt.sign({id: user['personId']},config.secret)
        response.send(utils.createResult(error,
          {
            personId:user['personId'],
            name: user['name'],
            emailId: user['emailId'],
            role: user['role'],
            token: token
          }
        ))
        
      }
    }
  })
})


// --------------------customer details signin POST-------------------
router.post('/customer_signin', (request, response) => {
  const { emailId, password } = request.body
  const statement = `select cd.custId,pd.personId,pd.name,pd.emailId from personal_details pd
                    INNER JOIN  customer_vehicle_details cd ON pd.personId=cd.personId
                    where pd.emailId = '${emailId}' and pd.password = '${crypto.SHA256(password)}' and pd.role='customer'`
  db.query(statement, (error, users) => {
    if (error) {
      response.send({ status: 'error', error: error })
    } else {
      if (users.length == 0) {
        // user does not exist
        response.send({ status: 'error', error: 'user does not exist' })
      } else {
        // user exists
        const user = users[0]
        const token = jwt.sign({id: user['personId']},config.secret)
        response.send(utils.createResult(error,
          {
            name: user['name'],
            emailId: user['emailId'],
            token: token
          }
        ))
        
      }
    }
  })
})


module.exports = router